package hr.java.covidportal.main;

import hr.java.covidportal.genericsi.KlinikaZaInfektivneBolesti;
import hr.java.covidportal.model.*;

import hr.java.covidportal.sort.VirusSort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Glavna {

    static final String FILENAMEZUPANIJE = "dat/zupanije.txt";
    static final String FILENAMESIMPTOMI = "dat/simptomi.txt";
    static final String FILENAMEBOLESTI = "dat/bolesti.txt";
    static final String FILENAMEVIRUSI = "dat/virusi.txt";
    static final String FILENAMEOSOBE = "dat/osobe.txt";

    ////////////////////////LABOS---------------------------------
    static final String FILENAMEFILTRIRANE = "dat/filtriraneOsobe.txt";
    static final String FILENAMESERIJALIZIRANE = "dat/serijaliziraneOsobe.txt";

    static final String SERIALIZATIONFILENAME = "dat/serijalizacija.dat";

    static final Integer BOLEST = 1;
    static final Integer VIRUS = 2;

    private static final Logger logger = LoggerFactory.getLogger(Glavna.class);


    public static void main(String[] args) {

        logger.info("Pocetak rada aplikacije!");

        Scanner input = new Scanner(System.in);

        List<Zupanija> zupanije = new ArrayList<>();
        dohvatiZupanije(zupanije);

        System.out.println("Ucitavanje podataka o simptomima...");
        List<Simptom> simptomi = new ArrayList<>();
        dohvatiSimptome(simptomi);

        System.out.println("Ucitavanje podataka o bolestima...");
        List<Bolest> bolesti = new ArrayList<>();
        dohvatiBolesti(bolesti, simptomi);

        System.out.println("Ucitavanje podataka o virusima...");
        List<Virus> virusi = new ArrayList<>();
        dohvatiViruse(virusi, simptomi);
        List<Bolest> listaSvihBolest = Stream.concat(bolesti.stream(), virusi.stream())
                .collect(Collectors.toList());

        System.out.println("Ucitavanje osoba...");
        List<Osoba> osobe = new ArrayList<>();
        dohvatiOsobe(osobe, zupanije, listaSvihBolest);

        ispisOsoba(osobe);
        ispisMapeVirusa(osobe);
        System.out.println("Najviše zaraženih osoba ima u županiji " + zupanije.get(0).getNaziv() + ": " + zupanije.get(0).getPostotakZarazenih() + "%");

        System.out.println("Virusi sortirani po nazivu suprotno od poretka abecede: ");
        virusi.stream()
                .sorted(Comparator.comparing(Virus::getNaziv).reversed())
                .collect(Collectors.toList());
        for (Virus v : virusi) {
            System.out.println(v.getNaziv());
        }

        KlinikaZaInfektivneBolesti<Virus, Osoba> mojaBolnica = new KlinikaZaInfektivneBolesti<>();
        dodavanjeUMapu(listaSvihBolest, osobe, mojaBolnica);

        mjerenjeVremena(mojaBolnica);

        System.out.print("Unesite string za pretragu po prezimenu: ");
        String stringPretraga = input.nextLine();
        ispisPodudaranihOsoba(osobe, stringPretraga);

        ispisBolestiSaSimptomima(listaSvihBolest);
        serijalizirajZupanije(zupanije);


        ////////////////PRVI ZADATAK------------------------------------
        List<Osoba> filtriraneOsobe = osobe.stream()
                .filter(o -> o.getIme().toLowerCase().startsWith("a") && o.getPrezime().toLowerCase().startsWith("r"))
                .collect(Collectors.toList());

        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(FILENAMESERIJALIZIRANE))) {
            out.writeObject(filtriraneOsobe);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        List<Osoba> procitaneOsobe = new ArrayList<>();
        Path datotekaOsobe = Path.of(FILENAMESERIJALIZIRANE);
        if (Files.exists(datotekaOsobe)) {
            try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(FILENAMESERIJALIZIRANE))) {
                procitaneOsobe = (List<Osoba>) in.readObject();
            } catch (IOException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        }
        procitaneOsobe.stream()
                .forEach(System.out::println);
        //System.out.println(procitaneOsobe);


        /////////////////DRUGI ZADATAK-----------------------------------
        Osoba osobaSaNajduljimPrezimenom = osobe.stream()
                .max(Comparator.comparing(korisnik -> korisnik.getPrezime().length()))
                .get();

        Osoba osobaSaNajkracimPrezimenom = osobe.stream()
                .min(Comparator.comparing(korisnik -> korisnik.getPrezime().length()))
                .get();

        int sumaPrezimenaOsoba = osobe.stream()
                .mapToInt(korisnik -> korisnik.getPrezime().length()).sum();

        double avgPrezimenaOsoba = osobe.stream()
                .mapToInt(korisnik -> korisnik.getPrezime().length())
                .average()
                .getAsDouble();

        Path datotekaZaPisanje = Path.of(FILENAMEFILTRIRANE);

        try {
            Files.writeString(datotekaZaPisanje, "Najdulje prezime: " + osobaSaNajduljimPrezimenom.getIme() + " " + osobaSaNajduljimPrezimenom.getPrezime());
            Files.writeString(datotekaZaPisanje, "\nNajkraće prezime: " + osobaSaNajkracimPrezimenom.getIme() + " " + osobaSaNajkracimPrezimenom.getPrezime(), StandardOpenOption.APPEND);
            Files.writeString(datotekaZaPisanje, "\nSuma duljine prezimena: " + sumaPrezimenaOsoba, StandardOpenOption.APPEND);
            Files.writeString(datotekaZaPisanje, "\nProsjek duljine prezimena: " + avgPrezimenaOsoba, StandardOpenOption.APPEND);

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        try {
            String tekst = Files.readString(datotekaZaPisanje);
            System.out.println(tekst);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    private static void dohvatiZupanije(List<Zupanija> zupanije) {
        System.out.println("Ucitavanje podataka o zupanijama...");
        try (BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(FILENAMEZUPANIJE), StandardCharsets.UTF_8))) {
            String linija = input.readLine();
            do {
                Long id = Long.parseLong(linija);
                String naziv = input.readLine();
                Integer brStanovnika = Integer.parseInt(input.readLine());
                Integer brZarazenih = Integer.parseInt(input.readLine());
                Zupanija tmp = new Zupanija(id, naziv, brStanovnika, brZarazenih);
                zupanije.add(tmp);
                linija = input.readLine();
            } while (linija != null);
        } catch (IOException e) {
            System.err.println("Pogreska kod ucitavanja datoteke " + FILENAMEZUPANIJE);
            e.printStackTrace();
        }
    }

    private static void dohvatiSimptome(List<Simptom> simptomi) {
        System.out.println("Ucitavanje podataka o simptomima...");
        try (BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(FILENAMESIMPTOMI), StandardCharsets.UTF_8))) {
            String linija = input.readLine();
            do {
                Long id = Long.parseLong(linija);
                String naziv = input.readLine();
                String vrijednost = input.readLine();
                Simptom tmp = new Simptom(id, naziv, vrijednost);
                simptomi.add(tmp);
                linija = input.readLine();
            } while (linija != null);
        } catch (IOException e) {
            System.err.println("Pogreska kod ucitavanja datoteke " + FILENAMESIMPTOMI);
            e.printStackTrace();
        }
    }

    private static void dohvatiBolesti(List<Bolest> bolesti, List<Simptom> simptomi) {
        System.out.println("Ucitavanje podataka o bolestima...");
        try (BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(FILENAMEBOLESTI), Charset.forName("UTF-8")))) {
            String linija = input.readLine();
            do {
                Long id = Long.parseLong(linija);
                String naziv = input.readLine();
                List<Simptom> tmpSimptomi = new ArrayList<>();
                String tmpsimp = input.readLine();
                for (char s : tmpsimp.toCharArray()) {
                    if (Character.isDigit(s)) {
                        Integer index = Character.getNumericValue(s);
                        tmpSimptomi.add(simptomi.get(index - 1));
                    }
                }
                Bolest tmp = new Bolest(id, naziv, tmpSimptomi);
                bolesti.add(tmp);
                linija = input.readLine();
            } while (linija != null);
        } catch (IOException e) {
            System.err.println("Pogreska kod ucitavanja datoteke " + FILENAMEBOLESTI);
            e.printStackTrace();
        }
    }

    private static void dohvatiViruse(List<Virus> virusi, List<Simptom> simptomi) {
        System.out.println("Ucitavanje podataka o virusima...");
        try (BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(FILENAMEVIRUSI), Charset.forName("UTF-8")))) {
            String linija = input.readLine();
            do {
                Long id = Long.parseLong(linija);
                String naziv = input.readLine();
                List<Simptom> tmpSimptomi = new ArrayList<>();
                String tmpsimp = input.readLine();
                for (char s : tmpsimp.toCharArray()) {
                    if (Character.isDigit(s)) {
                        Integer index = Character.getNumericValue(s);
                        tmpSimptomi.add(simptomi.get(index - 1));
                    }
                }
                Virus tmp = new Virus(id, naziv, tmpSimptomi);
                virusi.add(tmp);
                linija = input.readLine();
            } while (linija != null);
        } catch (IOException e) {
            System.err.println("Pogreska kod ucitavanja datoteke " + FILENAMEVIRUSI);
            e.printStackTrace();
        }
    }

    private static void dohvatiOsobe(List<Osoba> osobe, List<Zupanija> zupanije, List<Bolest> listaBolesti) {
        System.out.println("Ucitavanje podataka o virusima...");
        try (BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(FILENAMEOSOBE), Charset.forName("UTF-8")))) {
            String linija = input.readLine();
            int brojac = 0;
            do {
                Long id = Long.parseLong(linija);
                String ime = input.readLine();
                String prezime = input.readLine();
                Integer starost = Integer.parseInt(input.readLine());
                int index = Integer.parseInt(input.readLine());
                Zupanija zupanija = zupanije.get(index - 1);
                index = Integer.parseInt(input.readLine());
                Bolest bolest = listaBolesti.get(index - 1);
                List<Osoba> kontaktiraneOsobe = new ArrayList<>();
                if (brojac > 0) {
                    int brKontaktiranih = Integer.parseInt(input.readLine());
                    index = Integer.parseInt(input.readLine());
                    Osoba tmpKontakt = osobe.get(index - 1);
                    kontaktiraneOsobe.add(tmpKontakt);
                }
                Osoba tmp = new Osoba(id, ime, prezime, starost, zupanija, bolest, kontaktiraneOsobe);
                osobe.add(tmp);
                brojac++;
                linija = input.readLine();
            } while (linija != null);
        } catch (IOException e) {
            System.err.println("Pogreska kod ucitavanja datoteke " + FILENAMEOSOBE);
            e.printStackTrace();
        }
    }

    private static void dodavanjeUMapu(List<Bolest> bolesti, List<Osoba> osobe, KlinikaZaInfektivneBolesti<Virus, Osoba> mojaBolnica) {
        for (Bolest b : bolesti) {
            if (b instanceof Virus virus) {
                mojaBolnica.dodajVirus(virus);
            }
        }
        for (Osoba o : osobe) {
            if (o.getZarazenBolescu() instanceof Virus) {
                mojaBolnica.dodajOsobu(o);
            }
        }
    }

    private static void mjerenjeVremena(KlinikaZaInfektivneBolesti<Virus, Osoba> mojaBolnica) {
        Instant startTime = Instant.now();
        List<Virus> sortVirusiIzBolnice = mojaBolnica.getSviVirusi();
        sortVirusiIzBolnice.stream()
                .sorted(Comparator.comparing(Virus::getNaziv).reversed())
                .forEach(virus -> System.out.println(virus.getNaziv()));
        Instant stopTime = Instant.now();


        Instant pocetak = Instant.now();
        ;
        List<Virus> sortVirusiIzBolnice2 = mojaBolnica.getSviVirusi();
        sortVirusiIzBolnice2.sort(new VirusSort());
        for (Virus v : sortVirusiIzBolnice2) {
            System.out.println(v.getNaziv());
        }
        Instant zavrsetak = Instant.now();

        System.out.println("Sortiranje objekata korištenjem lambdi traje " + Duration.between(startTime, stopTime).toMillis() +
                " milisekundi, a bez lambda traje " + Duration.between(pocetak, zavrsetak).toMillis() + " milisekundi.");
    }

    private static void serijalizirajZupanije(List<Zupanija> zupanije) {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("dat/SerijalizacijaZupanija.dat"))) {
            List<Zupanija> filtriraneZupanije = zupanije.stream()
                    .filter(zupanija -> zupanija.getPostotakZarazenih() > 2)
                    .collect(Collectors.toList());
            out.writeObject(filtriraneZupanije);
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

    private static void ispisOsoba(List<Osoba> osobe) {
        System.out.println("Popis osoba: ");
        for (Osoba osoba : osobe) {
            System.out.println("Ime i prezime: " + osoba.getIme() + " " + osoba.getPrezime());
            System.out.println("Starost: " + osoba.getStarost());
            System.out.println("Županija prebivališta: " + osoba.getZupanija().getNaziv());
            System.out.println("Zaražen bolešću: " + osoba.getZarazenBolescu().getNaziv());
            System.out.println("Kontaktirane osobe:");
            if (osoba.getKontaktiraneOsobe().size() > 0) {
                for (int j = 0; j < osoba.getKontaktiraneOsobe().size(); j++) {
                    System.out.println(osoba.getKontaktiraneOsobe().get(j).getIme() + " " + osoba.getKontaktiraneOsobe().get(j).getPrezime());
                }
            } else {
                System.out.println("Nema kontaktiranih osoba!");
            }
        }
    }

    private static void ispisMapeVirusa(List<Osoba> osobe) {
        Map<Bolest, List<Osoba>> popisBolestiSaOsobama = new HashMap<>();
        popisBolestiSaOsobama = osobe.stream()
                .collect(Collectors.groupingBy(Osoba::getZarazenBolescu));
        AtomicInteger i = new AtomicInteger();
        AtomicInteger j = new AtomicInteger();
        popisBolestiSaOsobama.forEach((b, o) -> {
            System.out.println("Od virusa " + b.getNaziv() + " boluju:" + o.get(i.getAndIncrement()).getIme() + " " + o.get(j.getAndIncrement()).getPrezime());
        });
    }

    private static void ispisPodudaranihOsoba(List<Osoba> osobe, String stringPretraga) {
        System.out.println("Osobe cije prezime sadrzi " + stringPretraga + " su sljedece:");
        List<Osoba> lista = osobe.stream()
                .filter(osoba -> osoba.getPrezime().toLowerCase().contains(stringPretraga))
                .collect(Collectors.toList());
        Optional<List<Osoba>> opcionalnaOsoba = Optional.of(lista);
        opcionalnaOsoba.ifPresentOrElse(
                (osoba)
                        -> {
                    osoba.forEach(
                            osoba1 -> System.out.println(osoba1.getIme() + " " + osoba1.getPrezime()));
                },
                ()
                        -> System.out.print("Nema podudaranih osoba.")
        );
    }

    private static void ispisBolestiSaSimptomima(List<Bolest> bolesti) {
        List<String> popisSimptoma = bolesti.stream()
                .map(bolest -> bolest.getNaziv() + " ima " + bolest.getSimptomi().length + " simptoma")
                .collect(Collectors.toList());
        for (String poruka : popisSimptoma) {
            System.out.println(poruka);
        }
    }

}