package hr.java.covidportal.sort;

import hr.java.covidportal.model.Zupanija;

import java.util.Comparator;

public class CovidSorter implements Comparator<Zupanija>{
        @Override
        public int compare(Zupanija z1, Zupanija z2) {
            return z1.getPostotakZarazenih().compareTo(z2.getPostotakZarazenih());
            }
        }
