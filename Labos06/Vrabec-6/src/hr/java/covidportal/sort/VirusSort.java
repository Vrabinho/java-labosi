package hr.java.covidportal.sort;

import hr.java.covidportal.genericsi.KlinikaZaInfektivneBolesti;
import hr.java.covidportal.model.Virus;

import java.util.Comparator;

public class VirusSort extends KlinikaZaInfektivneBolesti implements Comparator<Virus> {
    @Override
    public int compare(Virus v1, Virus v2) {
        return v2.getNaziv().compareTo(v1.getNaziv());
    }
}