package hr.java.covidportal.genericsi;

import hr.java.covidportal.model.Osoba;
import hr.java.covidportal.model.Virus;

import java.util.ArrayList;
import java.util.List;

public class KlinikaZaInfektivneBolesti<T extends Virus, S extends Osoba> {

    List<T> sviVirusi = new ArrayList<>();
    List<S> zarazeneOsobe = new ArrayList<>();

    public KlinikaZaInfektivneBolesti(List<T> sviVirusi, List<S> zarazeneOsobe) {
        this.sviVirusi = sviVirusi;
        this.zarazeneOsobe = zarazeneOsobe;
    }

    public KlinikaZaInfektivneBolesti() {

    }

    public List<T> getSviVirusi() {
        return sviVirusi;
    }

    public void setSviVirusi(List<T> sviVirusi) {
        this.sviVirusi = sviVirusi;
    }

    public List<S> getZarazeneOsobe() {
        return zarazeneOsobe;
    }

    public void setZarazeneOsobe(List<S> zarazeneOsobe) {
        this.zarazeneOsobe = zarazeneOsobe;
    }

    public void dodajVirus(T virus){
        sviVirusi.add(virus);
    }

    public void dodajOsobu(S osoba){
        zarazeneOsobe.add(osoba);
    }
}
