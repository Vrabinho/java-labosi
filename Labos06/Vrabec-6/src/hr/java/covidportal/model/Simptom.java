package hr.java.covidportal.model;

import java.util.Objects;

/**
 * Sprema simptom i extenda <code>ImenaovaniEntitet</code>
 */
public class Simptom extends ImenovaniEntitet{


    String vrijednost;

    /**
     * konstruktor sa jednim parametrom
     * @param naziv prima naziv i salje ga u u konstruktor nadklase <code>ImenaovaniEntitet</code>
     * @param vrijednost prima string
     */
    public Simptom(Long id, String naziv, String vrijednost) {
        super(id, naziv);
        this.vrijednost = vrijednost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Simptom)) return false;
        Simptom simptom = (Simptom) o;
        return Objects.equals(getVrijednost(), simptom.getVrijednost());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getVrijednost());
    }

    public String getVrijednost() {
        return vrijednost;
    }

    public void setVrijednost(String vrijednost) {
        this.vrijednost = vrijednost;
    }
}
