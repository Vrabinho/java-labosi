package hr.java.covidportal.model;

import java.util.Arrays;
import java.util.List;


public class Bolest extends ImenovaniEntitet  {
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Bolest)) return false;
        Bolest bolest = (Bolest) o;
        return Arrays.equals(getSimptomi(), bolest.getSimptomi());
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(getSimptomi());
    }

    Simptom[] simptomi;

    /**
     * konstruktor sa dva parametra
     * @param naziv prima naziv bolesti koji saljemo u nadklasu
     * @param simptomi prima polje simptoma
     */

    public Bolest(Long id, String naziv, List<Simptom> simptomi) {
        super(id, naziv);
        this.simptomi = simptomi.toArray(new Simptom[0]);
    }


    public Simptom[] getSimptomi() {
        return simptomi;
    }

    public void setSimptomi(Simptom[] simptomi) {
        this.simptomi = simptomi;
    }
}
