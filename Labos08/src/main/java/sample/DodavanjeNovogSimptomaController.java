package main.java.sample;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ResourceBundle;


public class DodavanjeNovogSimptomaController implements Initializable {

    static final String FILESIMPTOMI = "dat/simptomi.txt";
    Path datotekaZaPisanje = Path.of(FILESIMPTOMI);
    ObservableValue<Integer> idSimptoma;

    @FXML
    private TextField nazivSimptoma;

    @FXML
    ChoiceBox<String> vrijednostSimptoma = new ChoiceBox<>();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        vrijednostSimptoma.getItems().addAll("RIJETKO", "SREDNJE", "ČESTO");
        vrijednostSimptoma.setValue("RIJETKO");

        try {
            BufferedReader reader = new BufferedReader(new FileReader(FILESIMPTOMI));
            int redak = 0;
            while (reader.readLine() != null) redak++;
            reader.close();
            int id = redak / 3 + 1;
            idSimptoma = new SimpleIntegerProperty(id).asObject();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void dohvatiBrojRedaka() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(FILESIMPTOMI));
            int redak = 0;
            while (reader.readLine() != null) redak++;
            reader.close();
            int id = redak / 3 + 1;
            idSimptoma = new SimpleIntegerProperty(id).asObject();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public String dohvatiVrijednostSimptoma(ChoiceBox<String> vrijednostSimptoma) {
        return vrijednostSimptoma.getValue();
    }

    @FXML
    public void spremiSimptom() {

        try {
            dohvatiBrojRedaka();
            String idSimptomaWrite = idSimptoma.getValue().toString();
            String nazivSimptomaWrite = nazivSimptoma.getText();
            String vrijednostSimptomaWrite = dohvatiVrijednostSimptoma(vrijednostSimptoma);

            Files.writeString(datotekaZaPisanje, "\n" + idSimptomaWrite, StandardOpenOption.APPEND);
            Files.writeString(datotekaZaPisanje, "\n" + nazivSimptomaWrite, StandardOpenOption.APPEND);
            Files.writeString(datotekaZaPisanje, "\n" + vrijednostSimptomaWrite, StandardOpenOption.APPEND);

            Alert dodaniSimptom = new Alert(Alert.AlertType.INFORMATION);
            dodaniSimptom.setTitle("Dodavanje novog simptoma");
            dodaniSimptom.setHeaderText("Uspješno dodan novi simptom");
            dodaniSimptom.setContentText("Simptom " + nazivSimptomaWrite + " uspješno spremljen u datoteku.");
            dodaniSimptom.showAndWait();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
