package main.java.sample;


import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import main.covidportal.enumeracija.Enumeracija;
import main.covidportal.model.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;


public class DodavanjeNoveOsobeController implements Initializable {

    static final String FILESIMPTOMI = "dat/simptomi.txt";
    static final String FILEVIRUSI = "dat/virusi.txt";
    static final String FILEBOLESTI = "dat/bolesti.txt";
    static final String FILEZUPANIJE = "dat/zupanije.txt";
    static final String FILEOSOBE = "dat/osobe.txt";
    Path datotekaZaPisanje = Path.of(FILEOSOBE);
    List<Zupanija> zupanije = new ArrayList<>();
    Set<Simptom> simptomi = new HashSet<>();
    Set<Bolest> bolesti = new HashSet<>();
    List<Osoba> osobe = new ArrayList<>();


    @FXML
    private TextField imeOsobe;

    @FXML
    private TextField prezimeOsobe;

    @FXML
    private TextField oibOsobe;

    @FXML
    private TextField starostOsobe;

    @FXML
    ChoiceBox<String> zupanijaOsobe = new ChoiceBox<>();

    @FXML
    ChoiceBox<String> bolestOsobe = new ChoiceBox<>();

    @FXML
    ListView<Osoba> osobaListView = new ListView<>();


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        zupanije.addAll(dohvatiZupanije());
        simptomi.addAll(dohvatiSimptome());
        bolesti.addAll(dohvatiBolesti(simptomi));
        bolesti.addAll(dohvatiViruse(simptomi));
        osobe.addAll(dohvatiOsobe(zupanije, bolesti));

        for (Zupanija zupanija : zupanije) {
            zupanijaOsobe.getItems().add(zupanija.getNaziv());
        }
        zupanijaOsobe.setValue("Odaberite županiju");

        for (Bolest bolest : bolesti) {
            bolestOsobe.getItems().add(bolest.getNaziv());
        }
        bolestOsobe.setValue("Odaberite bolest");

        osobaListView.getItems().addAll(osobe);
        osobaListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

    }


    private static Set<Simptom> dohvatiSimptome() {
        Set<Simptom> simptomi = new HashSet<>();
        try (BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(FILESIMPTOMI), StandardCharsets.UTF_8))) {
            String linija = null;
            while ((linija = input.readLine()) != null) {
                Long id = Long.parseLong(linija);
                String naziv = input.readLine();
                String vrijednostSimptoma = input.readLine();
                Enumeracija vrijednostSimptomaEnum = switch (vrijednostSimptoma) {
                    case "RIJETKO" -> Enumeracija.RIJETKO;
                    case "SREDNJE" -> Enumeracija.SREDNJE;
                    case "ČESTO" -> Enumeracija.CESTO;
                    default -> null;
                };
                Simptom tmpSimptom = new Simptom(id, naziv, vrijednostSimptomaEnum);
                simptomi.add(tmpSimptom);
            }

        } catch (IOException ex) {
            System.err.println("Pogreška kod čitanja iz datoteke: " + FILESIMPTOMI);
            ex.printStackTrace();
        }
        return simptomi;
    }

    private static List<Zupanija> dohvatiZupanije() {
        List<Zupanija> zupanije = new ArrayList<>();
        try (BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(FILEZUPANIJE), StandardCharsets.UTF_8))) {
            String linija = null;
            while ((linija = input.readLine()) != null) {
                Long id = Long.parseLong(linija);
                String naziv = input.readLine();
                Integer brojStanovnika = Integer.parseInt(input.readLine());
                Integer brojZarazenihStanovnika = Integer.parseInt(input.readLine());
                Zupanija tmpZupanija = new Zupanija(id, naziv, brojStanovnika, brojZarazenihStanovnika);
                zupanije.add(tmpZupanija);
            }
        } catch (IOException ex) {
            System.err.println("Pogreška kod čitanja iz datoteke: " + FILEZUPANIJE);
            ex.printStackTrace();
        }
        return zupanije;
    }

    private static Set<Bolest> dohvatiBolesti(Set<Simptom> simptomi) {
        Set<Bolest> bolesti = new HashSet<>();
        try (BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(FILEBOLESTI), StandardCharsets.UTF_8))) {
            String linija = null;
            while ((linija = input.readLine()) != null) {
                Long id = Long.parseLong(linija);
                String naziv = input.readLine();
                String idSimptoma = input.readLine();
                String[] poljeIdSimptoma = idSimptoma.split(",");
                List<Simptom> simptomiBolesti = new ArrayList<>();
                for (Simptom simptom : simptomi) {
                    for (String s : poljeIdSimptoma) {
                        Long tmpIdSimptoma = Long.parseLong(s);
                        if (simptom.getId().equals(tmpIdSimptoma)) {
                            simptomiBolesti.add(simptom);
                        }
                    }
                }
                Bolest tmpBolest = new Bolest(id, naziv, simptomiBolesti);
                bolesti.add(tmpBolest);
            }
        } catch (IOException ex) {
            System.err.println("Pogreška kod čitanja iz datoteke: " + FILEBOLESTI);
            ex.printStackTrace();
        }
        return bolesti;
    }

    private static Set<Bolest> dohvatiViruse(Set<Simptom> simptomi) {
        Set<Bolest> bolesti = new HashSet<>();
        try (BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(FILEVIRUSI), StandardCharsets.UTF_8))) {
            String linija = null;
            while ((linija = input.readLine()) != null) {
                Long id = Long.parseLong(linija);
                String naziv = input.readLine();
                String idSimptoma = input.readLine();
                String[] poljeIdSimptoma = idSimptoma.split(",");
                List<Simptom> simptomiBolesti = new ArrayList<>();
                for (Simptom simptom : simptomi) {
                    for (String s : poljeIdSimptoma) {
                        Long tmpIdSimptoma = Long.parseLong(s);
                        if (simptom.getId().equals(tmpIdSimptoma)) {
                            simptomiBolesti.add(simptom);
                        }
                    }
                }
                Virus tmpVirus = new Virus(id, naziv, simptomiBolesti);
                bolesti.add(tmpVirus);

            }
        } catch (IOException ex) {
            System.err.println("Pogreška kod čitanja iz datoteke: " + FILEVIRUSI);
            ex.printStackTrace();
        }
        return bolesti;
    }

    private static List<Osoba> dohvatiOsobe(List<Zupanija> zupanije, Set<Bolest> bolesti) {
        List<Osoba> osobe = new ArrayList<>();
        try (BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(FILEOSOBE), StandardCharsets.UTF_8))) {
            String linija = null;
            boolean prvaOsoba = true;
            while ((linija = input.readLine()) != null) {
                String prezimeOsobe = input.readLine();
                String oibOsobe = input.readLine();
                Integer starostOsobe = Integer.parseInt(input.readLine());
                Long idZupanije = Long.parseLong(input.readLine());
                Zupanija zupanijaOsobe = null;
                for (Zupanija zupanija : zupanije) {
                    if (zupanija.getId().equals(idZupanije)) {
                        zupanijaOsobe = zupanija;
                    }
                }
                Long idBolesti = Long.parseLong(input.readLine());
                Bolest bolestOsobe = null;
                for (Bolest bolest : bolesti) {
                    if (bolest.getId().equals(idBolesti)) {
                        bolestOsobe = bolest;
                    }
                }

                if (!prvaOsoba) {
                    String idKontaktiraneOsobe = input.readLine();
                    String[] poljeIdKontaktiraneOsobe = idKontaktiraneOsobe.split(",");
                    List<Osoba> kontaktiraneOsobe = new ArrayList<>();
                    for (String s : poljeIdKontaktiraneOsobe) {
                        Long tmpIdKontaktiraneOsobe = Long.parseLong(s);
                        kontaktiraneOsobe.add(osobe.get((int) (tmpIdKontaktiraneOsobe - 1)));
                    }
                    Osoba tmpOsoba = new Osoba.Builder(linija)
                            .saPrezimenom(prezimeOsobe)
                            .saOibom(oibOsobe)
                            .starosti(starostOsobe)
                            .izZupanije(zupanijaOsobe)
                            .imaBolest(bolestOsobe)
                            .saKontaktima(kontaktiraneOsobe)
                            .build();

                    osobe.add(tmpOsoba);
                } else {
                    List<Osoba> kontaktiraneOsobe = new ArrayList<>();
                    Osoba tmpOsoba = new Osoba.Builder(linija)
                            .saPrezimenom(prezimeOsobe)
                            .saOibom(oibOsobe)
                            .starosti(starostOsobe)
                            .izZupanije(zupanijaOsobe)
                            .imaBolest(bolestOsobe)
                            .build();

                    osobe.add(tmpOsoba);
                }
                prvaOsoba = false;

            }
        } catch (IOException ex) {
            System.err.println("Pogreška kod čitanja iz datoteke: " + FILEOSOBE);
            ex.printStackTrace();
        }

        return osobe;
    }


    public Long dohvatiZupanijuOsobe(ChoiceBox<String> zupanijeOsobe, List<Zupanija> zupanije) {
        String nazivZupanije = zupanijeOsobe.getValue();
        long id = 0;

        for (Zupanija zupanija : zupanije) {
            if (nazivZupanije.equals(zupanija.getNaziv())) {
                id = zupanija.getId();
            }
        }
        return id;
    }

    public Long dohvatiBolestOsobe(ChoiceBox<String> bolestOsobe, Set<Bolest> bolesti) {
        String nazivBolestiOsobe = bolestOsobe.getValue();
        long id = 0;

        for (Bolest bolest : bolesti) {
            if (nazivBolestiOsobe.equals(bolest.getNaziv())) {
                id = bolest.getId();
            }
        }
        return id;
    }


    public String dohvatiKontaktOsobe(ListView<Osoba> osobaListView, List<Osoba> osobe) {
        ObservableList<Osoba> odabraneKontaktOsobe = osobaListView.getSelectionModel().getSelectedItems();
        List<String> idOdabranihOsoba = new ArrayList<>();
        StringBuilder id = new StringBuilder();

        for (Osoba odabranaOsoba : odabraneKontaktOsobe) {
            int i = 1;
            for (Osoba osoba : osobe) {
                if (odabranaOsoba.getIme().equals(osoba.getIme()) && odabranaOsoba.getPrezime().equals(osoba.getPrezime())) {
                    idOdabranihOsoba.add(String.valueOf(i));
                }
                i++;
            }
        }

        for (String idOdabranih : idOdabranihOsoba) {
            id.append(idOdabranih).append(",");
        }

        id = new StringBuilder(id.substring(0, id.length() - 1));
        return id.toString();
    }

    @FXML
    public void spremiOsobu() {

        ////DRUGI ZADATAK//////

        boolean ispravnoIme = false;
        boolean ispravnoPrezime = false;
        boolean ispravnaStarost = false;
        boolean ispravanOib = false;
        boolean sveIspravno = false;

        if (imeOsobe.getText().trim().isEmpty()) {
            imeOsobe.getStylesheets().add(Objects.requireNonNull(getClass().getClassLoader().getResource("style.css")).toExternalForm());
            Alert imeOsobeAlert = new Alert(Alert.AlertType.ERROR);
            imeOsobeAlert.setTitle("Dodavanje nove osobe");
            imeOsobeAlert.setHeaderText("Pogrešan unos imena osobe!");
            imeOsobeAlert.showAndWait();
            imeOsobe.setStyle("-fx-background-color: red");
        } else ispravnoIme = true;


        if (prezimeOsobe.getText().trim().isEmpty()) {
            prezimeOsobe.getStylesheets().add(Objects.requireNonNull(getClass().getClassLoader().getResource("style.css")).toExternalForm());
            Alert prezimeOsobeAlert = new Alert(Alert.AlertType.ERROR);
            prezimeOsobeAlert.setTitle("Dodavanje nove osobe");
            prezimeOsobeAlert.setHeaderText("Pogrešan unos prezimena osobe!");
            prezimeOsobeAlert.showAndWait();
            prezimeOsobe.setStyle("-fx-background-color: red");
        } else ispravnoPrezime = true;


        if (oibOsobe.getText().trim().isEmpty()) {
            oibOsobe.getStylesheets().add(Objects.requireNonNull(getClass().getClassLoader().getResource("style.css")).toExternalForm());
            Alert oibOsobeAlert = new Alert(Alert.AlertType.ERROR);
            oibOsobeAlert.setTitle("Dodavanje nove osobe");
            oibOsobeAlert.setHeaderText("Pogršan unos oiba osobe!");
            oibOsobeAlert.showAndWait();
            oibOsobe.setStyle("-fx-background-color: red");
        } else ispravanOib = true;


        if (starostOsobe.getText().isEmpty()) {
            starostOsobe.getStylesheets().add(Objects.requireNonNull(getClass().getClassLoader().getResource("style.css")).toExternalForm());
            Alert starostOsobeAlert = new Alert(Alert.AlertType.ERROR);
            starostOsobeAlert.setTitle("Dodavanje nove osobe");
            starostOsobeAlert.setHeaderText("Pogršan unos starosti osobe!");
            starostOsobeAlert.showAndWait();
            starostOsobe.setStyle("-fx-background-color: red");
        } else ispravnaStarost = true;


        if (ispravnoIme && ispravnoPrezime && ispravanOib && ispravnaStarost) {

            sveIspravno = true;
        }


        if (sveIspravno) {

            try {
                String imeOsobeWrite = imeOsobe.getText();
                String prezimeOsobeWrite = prezimeOsobe.getText();
                String oibOsobeWrite = oibOsobe.getText();
                String starostOsobeWrite = starostOsobe.getText();
                String zupanijaOsobeWrite = dohvatiZupanijuOsobe(zupanijaOsobe, zupanije).toString();
                String bolestOsobeWrite = dohvatiBolestOsobe(bolestOsobe, bolesti).toString();
                String kontaktiraneOsobeWrite = dohvatiKontaktOsobe(osobaListView, osobe);

                Files.writeString(datotekaZaPisanje, "\n" + imeOsobeWrite, StandardOpenOption.APPEND);
                Files.writeString(datotekaZaPisanje, "\n" + prezimeOsobeWrite, StandardOpenOption.APPEND);
                Files.writeString(datotekaZaPisanje, "\n" + oibOsobeWrite, StandardOpenOption.APPEND);
                Files.writeString(datotekaZaPisanje, "\n" + starostOsobeWrite, StandardOpenOption.APPEND);
                Files.writeString(datotekaZaPisanje, "\n" + zupanijaOsobeWrite, StandardOpenOption.APPEND);
                Files.writeString(datotekaZaPisanje, "\n" + bolestOsobeWrite, StandardOpenOption.APPEND);
                Files.writeString(datotekaZaPisanje, "\n" + kontaktiraneOsobeWrite, StandardOpenOption.APPEND);

                Alert dodanaOsoba = new Alert(Alert.AlertType.INFORMATION);
                dodanaOsoba.setTitle("Dodavanje nove osobe");
                dodanaOsoba.setHeaderText("Uspješno dodana nova osoba");
                dodanaOsoba.setContentText("Osoba " + imeOsobeWrite + " " + prezimeOsobeWrite + " uspješno je spremljena u datoteku.");
                dodanaOsoba.showAndWait();


            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
