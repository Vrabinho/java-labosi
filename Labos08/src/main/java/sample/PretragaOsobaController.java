package main.java.sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import main.covidportal.enumeracija.Enumeracija;
import main.covidportal.model.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

public class PretragaOsobaController implements Initializable {
    static final String FILEZUPANIJE = "dat/zupanije.txt";
    static final String FILESIMPTOMI = "dat/simptomi.txt";
    static final String FILEBOLESTI = "dat/bolesti.txt";
    static final String FILEVIRUSI = "dat/virusi.txt";
    static final String FILEOSOBE = "dat/osobe.txt";
    private static ObservableList<Osoba> observableListaOsoba;
    private static final ObservableList<Osoba> filtriranaObservableListaOsoba = FXCollections.observableArrayList();
    Boolean prvoPokretanje = true;

    List<Zupanija> zupanije = new ArrayList<>();
    Set<Simptom> simptomi = new HashSet<>();
    Set<Bolest> bolesti = new HashSet<>();

    @FXML
    List<Osoba> osobe = new ArrayList<>();

    @FXML
    private TextField imeOsobe;

    @FXML
    private TextField prezimeOsobe;

    @FXML
    private TableView<Osoba> tablicaOsoba;

    @FXML
    private TableColumn<Osoba, String> stupacImeOsobe;

    @FXML
    private TableColumn<Osoba, String> stupacPrezimeOsobe;

    @FXML
    private TableColumn<Osoba, String> stupacOibOsobe;

    @FXML
    private TableColumn<Osoba, Integer> stupacStarostOsobe;

    @FXML
    private TableColumn<Osoba, Zupanija> stupacZupanijaOsobe;

    @FXML
    private TableColumn<Osoba, Bolest> stupacBolestiOsobe;

    @FXML
    private TableColumn<Osoba, List<Osoba>> stupacKontaktOsoba;


    private static void dohvatiZupanije(List<Zupanija> zupanije) {
        try (BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(FILEZUPANIJE), StandardCharsets.UTF_8))) {
            String linija = null;
            while ((linija = input.readLine()) != null) {
                Long id = Long.parseLong(linija);
                String naziv = input.readLine();
                Integer brojStanovnika = Integer.parseInt(input.readLine());
                Integer brojZarazenihStanovnika = Integer.parseInt(input.readLine());
                Zupanija tmpZupanija = new Zupanija(id, naziv, brojStanovnika, brojZarazenihStanovnika);
                zupanije.add(tmpZupanija);
            }
        } catch (IOException ex) {
            System.err.println("Pogreška kod čitanja iz datoteke: " + FILEZUPANIJE);
            ex.printStackTrace();
        }

    }

    private static void dohvatiSimptome(Set<Simptom> simptomi) {
        try (BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(FILESIMPTOMI), StandardCharsets.UTF_8))) {
            String linija = null;
            while ((linija = input.readLine()) != null) {
                Long id = Long.parseLong(linija);
                String naziv = input.readLine();
                String vrijednostSimptoma = input.readLine();
                Enumeracija vrijednostSimptomaEnum = switch (vrijednostSimptoma) {
                    case "RIJETKO" -> Enumeracija.RIJETKO;
                    case "SREDNJE" -> Enumeracija.SREDNJE;
                    case "ČESTO" -> Enumeracija.CESTO;
                    default -> null;
                };
                Simptom tmpSimptom = new Simptom(id, naziv, vrijednostSimptomaEnum);
                simptomi.add(tmpSimptom);
            }

        } catch (IOException ex) {
            System.err.println("Pogreška kod čitanja iz datoteke: " + FILESIMPTOMI);
            ex.printStackTrace();
        }

    }

    private static void dohvatiBolesti(Set<Bolest> bolesti, Set<Simptom> simptomi) {
        try (BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(FILEBOLESTI), StandardCharsets.UTF_8))) {
            String linija = null;
            while ((linija = input.readLine()) != null) {
                Long id = Long.parseLong(linija);
                String naziv = input.readLine();
                String idSimptoma = input.readLine();
                String[] poljeIdSimptoma = idSimptoma.split(",");
                List<Simptom> simptomiBolesti = new ArrayList<>();
                for (Simptom simptom : simptomi) {
                    for (String s : poljeIdSimptoma) {
                        Long tmpIdSimptoma = Long.parseLong(s);
                        if (simptom.getId().equals(tmpIdSimptoma)) {
                            simptomiBolesti.add(simptom);
                        }
                    }
                }
                Bolest tmpBolest = new Bolest(id, naziv, simptomiBolesti);
                bolesti.add(tmpBolest);

            }
        } catch (IOException ex) {
            System.err.println("Pogreška kod čitanja iz datoteke: " + FILEBOLESTI);
            ex.printStackTrace();
        }
    }

    private static void dohvatiViruse(Set<Bolest> bolesti, Set<Simptom> simptomi) {
        try (BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(FILEVIRUSI), StandardCharsets.UTF_8))) {
            String linija = null;
            while ((linija = input.readLine()) != null) {
                Long id = Long.parseLong(linija);
                String naziv = input.readLine();
                String idSimptoma = input.readLine();
                String[] poljeIdSimptoma = idSimptoma.split(",");
                List<Simptom> simptomiBolesti = new ArrayList<>();
                for (Simptom simptom : simptomi) {
                    for (String s : poljeIdSimptoma) {
                        Long tmpIdSimptoma = Long.parseLong(s);
                        if (simptom.getId().equals(tmpIdSimptoma)) {
                            simptomiBolesti.add(simptom);
                        }
                    }
                }
                Virus tmpVirus = new Virus(id, naziv, (List<Simptom>) simptomiBolesti);
                bolesti.add(tmpVirus);

            }
        } catch (IOException ex) {
            System.err.println("Pogreška kod čitanja iz datoteke: " + FILEVIRUSI);
            ex.printStackTrace();
        }
    }


    private static void dohvatiOsobe(List<Zupanija> zupanije, Set<Bolest> bolesti, List<Osoba> osobe) {
        try (BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(FILEOSOBE), StandardCharsets.UTF_8))) {
            String linija = null;
            boolean prvaOsoba = true;
            while ((linija = input.readLine()) != null) {
                String prezimeOsobe = input.readLine();
                String oibOsobe = input.readLine();
                Integer starostOsobe = Integer.parseInt(input.readLine());
                Long idZupanije = Long.parseLong(input.readLine());
                Zupanija zupanijaOsobe = null;
                for (Zupanija zupanija : zupanije) {
                    if (zupanija.getId().equals(idZupanije)) {
                        zupanijaOsobe = zupanija;
                    }
                }
                Long idBolesti = Long.parseLong(input.readLine());
                Bolest bolestOsobe = null;
                for (Bolest bolest : bolesti) {
                    if (bolest.getId().equals(idBolesti)) {
                        bolestOsobe = bolest;
                    }
                }

                if (!prvaOsoba) {
                    String idKontaktiraneOsobe = input.readLine();
                    String[] poljeIdKontaktiraneOsobe = idKontaktiraneOsobe.split(",");
                    List<Osoba> kontaktiraneOsobe = new ArrayList<>();
                    for (String s : poljeIdKontaktiraneOsobe) {
                        Long tmpIdKontaktiraneOsobe = Long.parseLong(s);
                        kontaktiraneOsobe.add(osobe.get((int) (tmpIdKontaktiraneOsobe - 1)));
                    }
                    Osoba tmpOsoba = new Osoba.Builder(linija)
                            .saPrezimenom(prezimeOsobe)
                            .saOibom(oibOsobe)
                            .starosti(starostOsobe)
                            .izZupanije(zupanijaOsobe)
                            .imaBolest(bolestOsobe)
                            .saKontaktima(kontaktiraneOsobe)
                            .build();

                    osobe.add(tmpOsoba);
                } else {
                    List<Osoba> kontaktiraneOsobe = new ArrayList<>();
                    Osoba tmpOsoba = new Osoba.Builder(linija)
                            .saPrezimenom(prezimeOsobe)
                            .saOibom(oibOsobe)
                            .starosti(starostOsobe)
                            .izZupanije(zupanijaOsobe)
                            .imaBolest(bolestOsobe)
                            .saKontaktima(kontaktiraneOsobe)
                            .build();

                    osobe.add(tmpOsoba);
                }
                prvaOsoba = false;

            }
        } catch (IOException ex) {
            System.err.println("Pogreška kod čitanja iz datoteke: " + FILEOSOBE);
            ex.printStackTrace();
        }

    }


    @FXML
    public void prikaziGlavniEkran() throws IOException {
        Parent glavniEkranFrame =
                FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("pocetniEkran.fxml")));
        Scene glavniEkranScena = new Scene(glavniEkranFrame, 700, 310);
        Main.getMainStage().setScene(glavniEkranScena);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        stupacImeOsobe.setCellValueFactory(new PropertyValueFactory<Osoba, String>("ime"));
        stupacPrezimeOsobe.setCellValueFactory(new PropertyValueFactory<Osoba, String>("prezime"));
        stupacOibOsobe.setCellValueFactory(new PropertyValueFactory<Osoba, String>("oib"));
        stupacStarostOsobe.setCellValueFactory(new PropertyValueFactory<Osoba, Integer>("starost"));
        stupacZupanijaOsobe.setCellValueFactory(new PropertyValueFactory<Osoba, Zupanija>("zupanija"));
        stupacBolestiOsobe.setCellValueFactory(new PropertyValueFactory<Osoba, Bolest>("zarazenBolescu"));
        stupacKontaktOsoba.setCellValueFactory(new PropertyValueFactory<Osoba, List<Osoba>>("kontaktiraneOsobe"));

        if (observableListaOsoba == null) {
            observableListaOsoba = FXCollections.observableArrayList();
        }
        if (prvoPokretanje) {
            observableListaOsoba.clear();
            dohvatiZupanije(zupanije);
            dohvatiSimptome(simptomi);
            dohvatiBolesti(bolesti, simptomi);
            dohvatiViruse(bolesti, simptomi);
            dohvatiOsobe(zupanije, bolesti, osobe);
            observableListaOsoba.addAll(osobe);
            tablicaOsoba.setItems(observableListaOsoba);
            prvoPokretanje = false;
        }

        ///TRECI ZADATAK
        tablicaOsoba.setRowFactory(tablicaOsoba -> {
            TableRow<Osoba> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    Osoba osoba = row.getItem();
                    doubleClick(osoba);
                }
            });
            return row;
        });
    }


    @FXML
    public void pretraziOsobe() {
        String ime = imeOsobe.getText();
        String prezime = prezimeOsobe.getText();

        List<Osoba> filtriranaListaOsoba = observableListaOsoba.stream()
                .filter(osoba -> osoba.getIme().toLowerCase().contains(ime))
                .filter(osoba -> osoba.getPrezime().toLowerCase().contains(prezime))
                .collect(Collectors.toList());

        filtriranaObservableListaOsoba.clear();
        filtriranaObservableListaOsoba.addAll(FXCollections.observableArrayList(filtriranaListaOsoba));
        tablicaOsoba.setItems(filtriranaObservableListaOsoba);
    }

    ///TRECI ZADATAK////
    public void doubleClick(Osoba osoba) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("DodavanjeNoveOsobe.fxml"));
            Scene scene = new Scene(loader.load(), 900, 500);
            Stage stage = new Stage();
            stage.setTitle("Editiranje osobe");
            stage.setScene(scene);
            stage.show();
            PrikazOsobeController controller = loader.getController();
            controller.prikaziOsobu(osoba);
            stupacImeOsobe.setCellValueFactory(new PropertyValueFactory<Osoba, String>(osoba.getIme()));
            stupacPrezimeOsobe.setCellValueFactory(new PropertyValueFactory<Osoba, String>(osoba.getPrezime()));
            stupacOibOsobe.setCellValueFactory(new PropertyValueFactory<Osoba, String>(osoba.getOib()));
            stupacStarostOsobe.setCellValueFactory(new PropertyValueFactory<Osoba, Integer>(osoba.getStarost().toString()));
            stupacZupanijaOsobe.setCellValueFactory(new PropertyValueFactory<Osoba, Zupanija>(osoba.getZupanija().getNaziv()));
            stupacBolestiOsobe.setCellValueFactory(new PropertyValueFactory<Osoba, Bolest>(osoba.getZarazenBolescu().getNaziv()));
            stupacKontaktOsoba.setCellValueFactory(new PropertyValueFactory<Osoba, List<Osoba>>(osoba.getKontaktiraneOsobe().toString()));

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
