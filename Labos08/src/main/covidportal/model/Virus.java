package main.covidportal.model;

import java.util.List;

/**
 * Extenda klasu <code>Bolest.java</code> i implenetira interface <code>Zarazno</code>
 */
public class Virus extends Bolest implements Zarazno {


    /**
     * konstruktor sa dva parametra
     *
     * @param naziv    prima naziv i salje ga u nadklasu <code>Bolest.java</code>
     * @param simptomi prima polje simptoma i salje ga u nadklasu <code>Bolest.java</code>
     */
    public Virus(Long id, String naziv, List<Simptom> simptomi) {
        super(id, naziv, simptomi);
    }


    /**
     * Prenosi zarazu na kontaktirane osobe
     * Kao parametar prima objekt <code>Osoba</code>
     *
     * @param osoba prima objekt <code>Osoba</code>
     */
    @Override
    public void prelazakZarazeNaOsobu(Osoba osoba) {
        osoba.setZarazenBolescu(this);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
