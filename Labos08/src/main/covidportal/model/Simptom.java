package main.covidportal.model;

import main.covidportal.enumeracija.Enumeracija;

import java.util.Objects;

/**
 * Sprema simptom i extenda <code>ImenaovaniEntitet</code>
 */
public class Simptom extends ImenovaniEntitet {


    Enumeracija vrijednost;

    /**
     * konstruktor sa jednim parametrom
     *
     * @param naziv      prima naziv i salje ga u u konstruktor nadklase <code>ImenaovaniEntitet</code>
     * @param vrijednost prima string
     */
    public Simptom(Long id, String naziv, Enumeracija vrijednost) {
        super(id, naziv);
        this.vrijednost = vrijednost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Simptom)) return false;
        Simptom simptom = (Simptom) o;
        return Objects.equals(getVrijednost(), simptom.getVrijednost());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getVrijednost());
    }

    public Enumeracija getVrijednost() {
        return vrijednost;
    }

    @Override
    public String toString() {
        return this.getNaziv() + " " + this.getVrijednost().getVrijednost();
    }

}
