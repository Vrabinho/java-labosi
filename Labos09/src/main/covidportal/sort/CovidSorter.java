package main.covidportal.sort;

import main.covidportal.model.Zupanija;

import java.util.Comparator;

public class CovidSorter implements Comparator<Zupanija> {
    @Override
    public int compare(Zupanija z1, Zupanija z2){
        return Integer.compare(z2.getPostotakZarazenih(), z1.getPostotakZarazenih());
    }
}
