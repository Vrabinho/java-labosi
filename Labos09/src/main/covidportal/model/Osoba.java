package main.covidportal.model;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

/**
 * Definira osobu sa njezinim parametrima i ostalim klasama
 */

public class Osoba {
    Long id;
    String ime;
    String prezime;
    Date datumRodjenja;
    Integer starost;
    Zupanija zupanija;
    Bolest zarazenBolescu;
    List<Osoba> kontaktiraneOsobe;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Osoba osoba)) return false;
        return getIme().equals(osoba.getIme()) &&
                getPrezime().equals(osoba.getPrezime()) &&
                getDatumRodjenja().equals(osoba.getDatumRodjenja()) &&
                getZupanija().equals(osoba.getZupanija()) &&
                getZarazenBolescu().equals(osoba.getZarazenBolescu()) &&
                Objects.equals(getKontaktiraneOsobe(), osoba.getKontaktiraneOsobe());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIme(), getPrezime(), getStarost(), getZupanija(), getZarazenBolescu(), getKontaktiraneOsobe());
    }

    /**
     * Zamjena za konstruktor, koristmo builder pattern.
     * Nisu nam potrebni svi parametri u unosu
     */
    public static class Builder {
        Long id;
        String ime;
        String prezime;
        Date datumRodjenja;
        Integer starost;
        Zupanija zupanija;
        Bolest zarazenBolescu;
        List<Osoba> kontaktiraneOsobe;


        public Builder(Long id) {
            this.id = id;
        }

        public Builder saImenom(String ime) {
            this.ime = ime;
            return this;
        }

        public Builder saPrezimenom(String prezime) {
            this.prezime = prezime;
            return this;
        }

        public Builder datumaRodjenja(Date datumRodjenja) {
            this.datumRodjenja = datumRodjenja;
            return this;
        }

        public Builder starosti(Date datumRodjenja) {
            this.starost = build().getStarost(datumRodjenja);
            return this;
        }

        public Builder izZupanije(Zupanija zupanija) {
            this.zupanija = zupanija;
            return this;
        }

        public Builder imaBolest(Bolest bol) {
            this.zarazenBolescu = bol;
            return this;
        }

        public Builder saKontaktima(List<Osoba> kontakti) {
            this.kontaktiraneOsobe = kontakti;
            return this;
        }

        public Osoba build() {
            Osoba osoba = new Osoba();
            osoba.id = this.id;
            osoba.ime = this.ime;
            osoba.prezime = this.prezime;
            osoba.datumRodjenja = this.datumRodjenja;
            osoba.starost = this.starost;
            osoba.zupanija = this.zupanija;
            osoba.kontaktiraneOsobe = this.kontaktiraneOsobe;
            osoba.zarazenBolescu = this.zarazenBolescu;
            return osoba;
        }
    }

    public Integer getStarost(Date datumRodjenja) {
        datumRodjenja = this.datumRodjenja;
        Date datumIzracun;
        LocalDate datumSada = LocalDate.now();

        datumIzracun = convertToDateViaInstant(datumSada);

        Calendar godina1 = getCalendar(datumRodjenja);
        Calendar godina2 = getCalendar(datumIzracun);

        int diff = godina1.get(Calendar.YEAR) - godina2.get(Calendar.YEAR);
        if (godina1.get(Calendar.MONTH) > godina2.get(Calendar.MONTH) ||
                (godina1.get(Calendar.MONTH) == godina2.get(Calendar.MONTH) && godina1.get(Calendar.DATE) > godina2.get(Calendar.DATE))) {
            diff--;
        }
        return diff *= -1;
    }

    public static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);
        return cal;
    }

    public Date convertToDateViaInstant(LocalDate dateToConvert) {
        return java.util.Date.from(dateToConvert.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }

    public Osoba() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public Date getDatumRodjenja() {
        return datumRodjenja;
    }

    public void setDatumRodjenja(Date datumRodjenja) {
        this.datumRodjenja = datumRodjenja;
    }

    public Integer getStarost() {
        return starost;
    }

    public void setStarost(Integer starost) {
        this.starost = starost;
    }

    public Zupanija getZupanija() {
        return zupanija;
    }

    public void setZupanija(Zupanija zupanija) {
        this.zupanija = zupanija;
    }

    public Bolest getZarazenBolescu() {
        return zarazenBolescu;
    }

    public void setZarazenBolescu(Bolest zarazenBolescu) {
        this.zarazenBolescu = zarazenBolescu;
    }

    public List<Osoba> getKontaktiraneOsobe() {
        return kontaktiraneOsobe;
    }

    public void setKontaktiraneOsobe(List<Osoba> kontaktiraneOsobe) {
        this.kontaktiraneOsobe = kontaktiraneOsobe;
    }

    public void setKontaktiranaOsoba(Osoba kontaktiranaOsoba) {

        if (this.kontaktiraneOsobe == null) {
            this.kontaktiraneOsobe = new ArrayList<>();
        }

        if (kontaktiranaOsoba == null) {
            return;
        }

        this.kontaktiraneOsobe.add(kontaktiranaOsoba);
    }

    @Override
    public String toString() {
        return this.ime + " " + this.prezime;
    }
}

