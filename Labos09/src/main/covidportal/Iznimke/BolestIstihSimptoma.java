package main.covidportal.Iznimke;

/**
 * Usporedba bolesti
 */

public class BolestIstihSimptoma extends RuntimeException {

    /**
     * konstruktor sa jednim parametrom
     * @param message prima poruku iznimke
     */
    public BolestIstihSimptoma(String message) {
        super(message);
    }

    /**
     * konstruktor sa dva parametra
     * @param message prima poruku iznimke
     * @param cause prima uzrok iznimke
     */
    public BolestIstihSimptoma(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * konstruktor sa jednim parametrom
     * @param cause prima uzrok iznimke
     */
    public BolestIstihSimptoma(Throwable cause) {
        super(cause);
    }
}
