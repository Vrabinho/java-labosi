package main.java.sample;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import main.covidportal.baza.BazaPodataka;
import main.covidportal.enumeracija.Enumeracija;
import main.covidportal.model.Bolest;
import main.covidportal.model.Simptom;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.util.*;


public class DodavanjeNovogVirusaController implements Initializable {

    static final String FILESIMPTOMI = "dat/simptomi.txt";
    static final String FILEVIRUSI = "dat/virusi.txt";
    Path datotekaZaPisanje = Path.of(FILEVIRUSI);
    ObservableValue<Integer> idVirusa;
    Set<Simptom> simptomi = dohvatiSimptome();


    @FXML
    private TextField nazivVirusa;

    @FXML
    ListView<Simptom> simptomListView = new ListView<>();


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        simptomListView.getItems().addAll(simptomi);
        simptomListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        try (BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(FILEVIRUSI), StandardCharsets.UTF_8))) {
            String linija = null;
            int id = 0;
            while ((linija = input.readLine()) != null) {
                id = Integer.parseInt(linija);
                input.readLine();
                input.readLine();
            }
            id++;
            idVirusa = new SimpleIntegerProperty(id).asObject();

        } catch (IOException | NullPointerException ex) {
            ex.printStackTrace();
        }
    }

    public void dohvatiBrojRedaka() {
        try (BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(FILEVIRUSI), StandardCharsets.UTF_8))) {
            String linija = null;
            Integer id = null;
            while ((linija = input.readLine()) != null) {
                id = Integer.parseInt(linija);
                input.readLine();
                input.readLine();
            }
            id++;
            idVirusa = new SimpleIntegerProperty(id).asObject();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private static Set<Simptom> dohvatiSimptome() {
        Set<Simptom> simptomi = new HashSet<>();
        try (BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(FILESIMPTOMI), StandardCharsets.UTF_8))) {
            String linija = null;
            while ((linija = input.readLine()) != null) {
                Long id = Long.parseLong(linija);
                String naziv = input.readLine();
                String vrijednostSimptoma = input.readLine();
                Enumeracija vrijednostSimptomaEnum = switch (vrijednostSimptoma) {
                    case "RIJETKO" -> Enumeracija.RIJETKO;
                    case "SREDNJE" -> Enumeracija.SREDNJE;
                    case "ČESTO" -> Enumeracija.CESTO;
                    default -> null;
                };
                Simptom tmpSimptom = new Simptom(id, naziv, vrijednostSimptomaEnum);
                simptomi.add(tmpSimptom);
            }

        } catch (IOException ex) {
            System.err.println("Pogreška kod citanja iz datoteke: " + FILESIMPTOMI);
            ex.printStackTrace();
        }

        return simptomi;
    }

    public String dohvatiVrijednostSimptoma(ListView<Simptom> simptomListView, Set<Simptom> simptomi) {
        ObservableList<Simptom> odabraniSimptomi = simptomListView.getSelectionModel().getSelectedItems();
        List<String> idOdabranihSimptoma = new ArrayList<>();
        StringBuilder id = new StringBuilder();

        for (Simptom odabraniSimptom : odabraniSimptomi) {
            for (Simptom simptom : simptomi) {
                if (odabraniSimptom.getNaziv().equals(simptom.getNaziv())) {
                    idOdabranihSimptoma.add(simptom.getId().toString());
                }
            }
        }

        for (String idOdabranih : idOdabranihSimptoma) {
            id.append(idOdabranih).append(",");
        }

        id = new StringBuilder(id.substring(0, id.length() - 1));
        return id.toString();
    }

    public List<Simptom> dohvatiVrijednostSimptomaList(ListView<Simptom> simptomListView, Set<Simptom> simptomi) {
        ObservableList<Simptom> odabraniSimptomi = simptomListView.getSelectionModel().getSelectedItems();
        List<Simptom> idOdabranihSimptoma = new ArrayList<>();
        StringBuilder id = new StringBuilder();

        for (Simptom odabraniSimptom : odabraniSimptomi) {
            for (Simptom simptom : simptomi) {
                if (odabraniSimptom.getNaziv().equals(simptom.getNaziv())) {
                    idOdabranihSimptoma.add(simptom);
                }
            }
        }

        return idOdabranihSimptoma;
    }

    @FXML
    public void spremiVirus() {

        try {
            dohvatiBrojRedaka();
            String idVirusaWrite = idVirusa.getValue().toString();
            String nazivVirusaWrite = nazivVirusa.getText();
            String vrijednostSimptomaWrite = dohvatiVrijednostSimptoma(simptomListView, simptomi);
            List<Simptom> simptomiLista = dohvatiVrijednostSimptomaList(simptomListView, simptomi);

            Files.writeString(datotekaZaPisanje, "\n" + idVirusaWrite, StandardOpenOption.APPEND);
            Files.writeString(datotekaZaPisanje, "\n" + nazivVirusaWrite, StandardOpenOption.APPEND);
            Files.writeString(datotekaZaPisanje, "\n" + vrijednostSimptomaWrite, StandardOpenOption.APPEND);

            Bolest b = new Bolest(new Long(idVirusa.getValue()), nazivVirusaWrite, simptomiLista);
            BazaPodataka.spremiNovuBolest(b);

            Alert dodaniSimptom = new Alert(Alert.AlertType.INFORMATION);
            dodaniSimptom.setTitle("Dodavanje novog virusa");
            dodaniSimptom.setHeaderText("Uspješno dodan novi virus");
            dodaniSimptom.setContentText("Virus " + nazivVirusaWrite + " uspješno spremljen u datoteku.");
            dodaniSimptom.showAndWait();


        } catch (IOException | SQLException ex) {
            ex.printStackTrace();
        }
    }
}
