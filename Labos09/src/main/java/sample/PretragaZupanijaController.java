package main.java.sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import main.covidportal.baza.BazaPodataka;
import main.covidportal.model.Zupanija;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class PretragaZupanijaController implements Initializable {
    private static ObservableList<Zupanija> observableListaZupanija;
    private static final ObservableList<Zupanija> filtriranaObservableListaZupanija = FXCollections.observableArrayList();
    Boolean prvoPokretanje = true;

    @FXML
    List<Zupanija> zupanije = new ArrayList<>();

    @FXML
    private TextField nazivZupanije;

    @FXML
    private TableView<Zupanija> tablicaZupanija;

    @FXML
    private TableColumn<Zupanija, String> stupacNazivZupanije;

    @FXML
    private TableColumn<Zupanija, Integer> stupacBrojStanovnika;

    @FXML
    private TableColumn<Zupanija, Integer> stupacBrojZarazenih;


    private void dohvatiZupanijeBaza() {
        try {
            this.zupanije = BazaPodataka.dohvatiSveZupanijeIzBaze();
        } catch (SQLException | IOException throwables) {
            throwables.printStackTrace();
        }
    }


    @FXML
    public void prikaziGlavniEkran() throws IOException {
        Parent glavniEkranFrame =
                FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("pocetniEkran.fxml")));
        Scene glavniEkranScena = new Scene(glavniEkranFrame, 700, 310);
        Main.getMainStage().setScene(glavniEkranScena);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        stupacNazivZupanije.setCellValueFactory(new PropertyValueFactory<Zupanija, String>("naziv"));
        stupacBrojStanovnika.setCellValueFactory(new PropertyValueFactory<Zupanija, Integer>("brojStanovnika"));
        stupacBrojZarazenih.setCellValueFactory(new PropertyValueFactory<Zupanija, Integer>("brojZarazenih"));

        if (observableListaZupanija == null) {
            observableListaZupanija = FXCollections.observableArrayList();
        }
        if (prvoPokretanje) {
            observableListaZupanija.clear();
            dohvatiZupanijeBaza();
            observableListaZupanija.addAll(zupanije);
            tablicaZupanija.setItems(observableListaZupanija);
            prvoPokretanje = false;

            /////PRVI ZADATAK////////
            ContextMenu cm = new ContextMenu();
            MenuItem mi1 = new MenuItem("Izbriši županiju");
            cm.getItems().add(mi1);

            tablicaZupanija.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent t) {
                    if (t.getButton() == MouseButton.SECONDARY) {
                        cm.show(tablicaZupanija, t.getScreenX(), t.getScreenY());
                    }
                }

            });

            mi1.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {

                    Zupanija z = tablicaZupanija.getSelectionModel().getSelectedItem();

                    Connection veza = BazaPodataka.otvoriBazu();

                    try {
                        PreparedStatement upit = veza.prepareStatement("DELETE FROM KONTAKTIRANE_OSOBE \n" +
                                "\n" +
                                "WHERE OSOBA_ID IN\n" +
                                "\n" +
                                "(SELECT ID FROM OSOBA WHERE ZUPANIJA_ID = ?)\n" +
                                "\n" +
                                "OR KONTAKTIRANA_OSOBA_ID IN\n" +
                                "\n" +
                                "(SELECT ID FROM OSOBA WHERE ZUPANIJA_ID = ?);\n" +
                                "\n" +
                                "DELETE FROM OSOBA WHERE ZUPANIJA_ID = ?;\n" +
                                "\n" +
                                "DELETE FROM zupanija WHERE ID = ?;");

                        upit.setLong(1, z.getId());
                        upit.setLong(2, z.getId());
                        upit.setLong(3, z.getId());
                        upit.setLong(4, z.getId());

                        upit.executeUpdate();

                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                    BazaPodataka.zatvoriVezuNaBazu(veza);
                }
            });

            /////DRUGI ZADATAK ____ NASTAVAK ISPOD////
            tablicaZupanija.setRowFactory(tablicaBolesti -> {
                TableRow<Zupanija> row = new TableRow<>();
                row.setOnMouseClicked(event -> {
                    if (event.getClickCount() == 2 && (!row.isEmpty())) {
                        Zupanija zupanija = row.getItem();
                        try {
                            doubleClick(zupanija);
                        } catch (SQLException | IOException throwables) {
                            throwables.printStackTrace();
                        }
                    }
                });
                return row;
            });
        }
    }

    public static ObservableList<Zupanija> getListaZupanija() {
        return observableListaZupanija;
    }

    public static void setListaZupanija(ObservableList<Zupanija> observableList) {
        observableListaZupanija = observableList;
    }

    @FXML
    public void pretraziZupanije() {
        String naziv = nazivZupanije.getText();

        List<Zupanija> filtriranaListaZupanija = observableListaZupanija.stream()
                .filter(zupanija -> zupanija.getNaziv().toLowerCase().contains(naziv))
                .collect(Collectors.toList());

        filtriranaObservableListaZupanija.clear();
        filtriranaObservableListaZupanija.addAll(FXCollections.observableArrayList(filtriranaListaZupanija));
        tablicaZupanija.setItems(filtriranaObservableListaZupanija);
    }


    ////DRUGI ZADATAK//////
    public void doubleClick(Zupanija zupanija) throws SQLException, IOException {
        Long id = zupanija.getId();
        String stringZaIspis = "";

        Connection veza = BazaPodataka.otvoriBazu();

        PreparedStatement upit = veza.prepareStatement("SELECT OSOBA.* FROM OSOBA WHERE ZUPANIJA_ID = ?;");
        upit.setLong(1, id);
        ResultSet rs = upit.executeQuery();

        Alert sveOsobe = new Alert(Alert.AlertType.INFORMATION);
        sveOsobe.setTitle("Osobe koje zive u  " + zupanija.getNaziv());
        sveOsobe.setHeaderText("Osobe koje zive u " + zupanija.getNaziv());

        while (rs.next()) {
            String ime = rs.getString("ime");
            String prezime = rs.getString("Prezime");

            stringZaIspis += ime + " " + prezime + "\n";
        }
        sveOsobe.setContentText(stringZaIspis);
        sveOsobe.showAndWait();

        BazaPodataka.zatvoriVezuNaBazu(veza);
    }
}
