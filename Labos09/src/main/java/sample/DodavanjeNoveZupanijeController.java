package main.java.sample;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import main.covidportal.baza.BazaPodataka;
import main.covidportal.model.Zupanija;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.util.ResourceBundle;


public class DodavanjeNoveZupanijeController implements Initializable {

    static final String FILEZUPANIJE = "dat/zupanije.txt";
    Path datotekaZaPisanje = Path.of(FILEZUPANIJE);
    ObservableValue<Integer> idZupanije;


    @FXML
    private TextField nazivZupanije;

    @FXML
    private TextField brojStanovnikaZupanije;

    @FXML
    private TextField brojZarazenihStanovnikaZupanije;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(FILEZUPANIJE));
            int redak = 0;
            while (reader.readLine() != null) redak++;
            reader.close();

            int id = redak / 3;
            idZupanije = new SimpleIntegerProperty(id).asObject();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void dohvatiBrojRedaka() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(FILEZUPANIJE));
            int redak = 0;
            while (reader.readLine() != null) redak++;
            reader.close();

            int id = redak / 3;
            idZupanije = new SimpleIntegerProperty(id).asObject();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    public void spremiZupaniju() {

        try {
            dohvatiBrojRedaka();
            String idZupanijeWrite = idZupanije.getValue().toString();
            String nazivZupanijeWrite = nazivZupanije.getText();
            String brojStanovnikaZupanijeWrite = brojStanovnikaZupanije.getText();
            String brojZarazenihStanovnikaZupanijeWrite = brojZarazenihStanovnikaZupanije.getText();

            Files.writeString(datotekaZaPisanje, "\n" + idZupanijeWrite, StandardOpenOption.APPEND);
            Files.writeString(datotekaZaPisanje, "\n" + nazivZupanijeWrite, StandardOpenOption.APPEND);
            Files.writeString(datotekaZaPisanje, "\n" + brojStanovnikaZupanijeWrite, StandardOpenOption.APPEND);
            Files.writeString(datotekaZaPisanje, "\n" + brojZarazenihStanovnikaZupanijeWrite, StandardOpenOption.APPEND);

            Zupanija z = new Zupanija(new Long(idZupanijeWrite), nazivZupanijeWrite, Integer.parseInt(brojStanovnikaZupanijeWrite), Integer.parseInt(brojZarazenihStanovnikaZupanijeWrite));
            BazaPodataka.spremiNovuZupaniju(z);

            Alert dodanaZupanija = new Alert(Alert.AlertType.INFORMATION);
            dodanaZupanija.setTitle("Dodavanje nove županije");
            dodanaZupanija.setHeaderText("Uspješno dodana nova županija");
            dodanaZupanija.setContentText("Županija " + nazivZupanijeWrite + " uspješno spremljena u datoteku.");
            dodanaZupanija.showAndWait();


        } catch (IOException | SQLException ex) {
            ex.printStackTrace();
        }
    }
}
