package main.covidportal.Iznimke;

/**
 * Usporedba kontaktiranih osoba
 */

public class BrojKontaktiranihOsoba extends Exception{

    /**
     * konstruktor sa jednim parametrom
     * @param message prima poruku iznimke
     */
    public BrojKontaktiranihOsoba(String message) {
        super(message);
    }

    /**
     * konstruktor sa dva parametra
     * @param message prima poruku iznimke
     * @param cause prima uzrok iznimke
     */
    public BrojKontaktiranihOsoba(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * konstruktor sa jednim parametrom
     * @param cause prima uzrok iznimke
     */
    public BrojKontaktiranihOsoba(Throwable cause) {
        super(cause);
    }
}
