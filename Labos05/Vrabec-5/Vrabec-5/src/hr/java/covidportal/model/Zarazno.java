package hr.java.covidportal.model;

/**
 * Prenosi bolest na druge osobe ako je bolest virus
 */

public interface Zarazno {

   public void prelazakZarazeNaOsobu(Osoba osoba);

}
