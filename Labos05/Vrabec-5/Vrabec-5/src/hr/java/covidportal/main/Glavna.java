package hr.java.covidportal.main;

import hr.java.covidportal.Iznimke.BolestIstihSimptoma;
import hr.java.covidportal.Iznimke.BrojKontaktiranihOsoba;
import hr.java.covidportal.Iznimke.DuplikatKontaktiraneOsobe;
import hr.java.covidportal.genericsi.KlasaZaViruse;
import hr.java.covidportal.genericsi.KlinikaZaInfektivneBolesti;
import hr.java.covidportal.model.*;
import hr.java.covidportal.sort.CovidSorter;
import hr.java.covidportal.sort.VirusSort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class Glavna {

    static int BROJ_ZUPANIJA;
    static int BROJ_OSOBA;
    static int BROJ_SIMPTOMA;
    static int BROJ_BOLESTI;

    private static final Logger logger = LoggerFactory.getLogger(Glavna.class);


    public static void main(String[] args){

        Map<Bolest, List<Osoba>> popisBolestiSaOsobama = new HashMap<>();

        Scanner unos = new Scanner(System.in);


        Set<Zupanija> zupanije = new HashSet<>();
        System.out.print("Unesite broj županija koje želite unijeti: ");
        int brojZupanija = unos.nextInt();
        unos.nextLine();
        BROJ_ZUPANIJA = brojZupanija;

        System.out.println("Unesite podatke o " + brojZupanija + " županije: ");
        for (int i = 0; i < BROJ_ZUPANIJA; i++) {
            zupanije.add(unosZupanije(unos));
        }


        Set<Simptom> simptomi = new HashSet<>();
        System.out.print("Unesite broj simptoma koje želite unijeti: ");
        int brojSimptoma = unos.nextInt();
        unos.nextLine();
        BROJ_SIMPTOMA = brojSimptoma;

        System.out.println("Unesite podatke o " + brojSimptoma + " simptoma");
        for (int i = 0; i < BROJ_SIMPTOMA; i++) {
            simptomi.add(unosSimptoma(unos));
        }


        Set<Bolest> bolesti = new HashSet<>();
        System.out.print("Unesite broj bolesti koje želite unijeti: ");
        int brojBolesti = unos.nextInt();
        unos.nextLine();

        System.out.print("Unesite broj virusa koje želite unijeti: ");
        int brojVirusa = unos.nextInt();
        unos.nextLine();
        BROJ_BOLESTI = brojBolesti+brojVirusa;

        System.out.println("Unesite podatke o " + BROJ_BOLESTI + " bolesti:");
        boolean neispravnaBolest = false;
        for (int i = 0; i < BROJ_BOLESTI; i++) {
                do {
                    bolesti.add(unosBolesti(unos, simptomi));
                    Bolest tmp = bolesti.iterator().next();
                    if (i > 0) {
                        try {
                            //provjeraIsteBolesti(bolesti, tmp);
                            neispravnaBolest = false;
                        } catch (BolestIstihSimptoma iznimka) {
                            System.out.println(iznimka.getMessage());
                            logger.error(iznimka.getMessage());
                            unos.nextLine();
                            neispravnaBolest = true;
                        }
                    }
                }while (neispravnaBolest);
        }

        List<Osoba> osobe = new ArrayList<>();
        System.out.print("Unesite broj osoba koje želite unijeti: ");
        int brojOsoba = unos.nextInt();
        unos.nextLine();
        BROJ_OSOBA = brojOsoba;

        for (int i = 0; i < BROJ_OSOBA; i++) {
            osobe.add(unosOsobe(unos, bolesti, zupanije, i));
            if (i > 0) {
                unosKontaktiranihOsoba(unos, osobe, i);
            }
        }
        System.out.print("Unesite string za pretragu po prezimenu: ");
        String stringPretraga = unos.nextLine();
        ispisOsoba(osobe);


        List<Zupanija> listaZupanija = new ArrayList<>(zupanije);
        listaZupanija.sort(new CovidSorter());
        System.out.println("Najviše zaraženih osoba ima u županiji " + listaZupanija.get(0).getNaziv() + ": " + listaZupanija.get(0).getPostotakZarazenih() +"%");

        KlinikaZaInfektivneBolesti<Virus, Osoba> mojaBolnica = new KlinikaZaInfektivneBolesti<>();

        for(Bolest b : bolesti){
            if(b instanceof Virus virus){
                mojaBolnica.dodajVirus(virus);
            }
        }

        for(Osoba o : osobe){
            if(o.getZarazenBolescu() instanceof Virus){
                mojaBolnica.dodajOsobu(o);
            }
        }

        Instant startTime = Instant.now();
        List<Virus> sortVirusiIzBolnice = mojaBolnica.getSviVirusi();
        sortVirusiIzBolnice.stream()
                .sorted(Comparator.comparing(Virus::getNaziv).reversed())
                .forEach(virus -> System.out.println(virus.getNaziv()));
        Instant stopTime = Instant.now();


        Instant pocetak = Instant.now();;
        List<Virus> sortVirusiIzBolnice2 = mojaBolnica.getSviVirusi();
        sortVirusiIzBolnice2.sort(new VirusSort());
        for( Virus v : sortVirusiIzBolnice2){
            System.out.println(v.getNaziv());
        }
        Instant zavrsetak = Instant.now();

        System.out.println("Sortiranje objekata korištenjem lambdi traje "+ Duration.between(startTime, stopTime).toMillis() +
                " milisekundi, a bez lambda traje " + Duration.between(pocetak, zavrsetak).toMillis() + " milisekundi.");


        popisBolestiSaOsobama = osobe.stream()
                .collect(Collectors.groupingBy(Osoba::getZarazenBolescu));
        System.out.println("Ispis mape virusa sa osobama: ");
        AtomicInteger i = new AtomicInteger();
        AtomicInteger j = new AtomicInteger();
        popisBolestiSaOsobama.forEach((b, o) -> {
            System.out.println("Od virusa " + b.getNaziv() + " boluju:" + o.get(i.getAndIncrement()).getIme() + " " + o.get(j.getAndIncrement()).getPrezime());
        });

        System.out.println("Osobe cije prezime sadrzi " + stringPretraga + " su sljedece:");
        List<Osoba> lista = osobe.stream()
                .filter(o -> o.getPrezime().contains(stringPretraga))
                .collect(Collectors.toList());

        Optional<List<Osoba>> opcionalnaOsoba = Optional.of(lista);
        opcionalnaOsoba.ifPresentOrElse(
                (osoba)
                        -> {osoba.forEach(
                                osoba1 -> System.out.println(osoba1.getIme() + " " + osoba1.getPrezime()));},
                ()
                        -> {System.out.print("Nema podudaranih osoba.");}
        );

        List<String> popisSimptoma =  bolesti.stream()
                .map(bolest -> bolest.getNaziv() + " ima " + bolest.getSimptomi().length + " simptoma")
                .collect(Collectors.toList());
        for(String poruka : popisSimptoma){
            System.out.println(poruka);
        }


        /////////////////////PRVI  ZADATAK--------------------------------------------
        List<Bolest> sortiraniVirusi = bolesti.stream()
                .filter(bolest -> bolest.getNaziv().toLowerCase().startsWith("c") && bolest.getNaziv().contains("19") && bolest instanceof Virus)
                .sorted(Comparator.comparing(Bolest::getNaziv))
                .collect(Collectors.toList());
        for(Bolest naziv : sortiraniVirusi){
            System.out.println(naziv.getNaziv());
        }

        //////DRUGI ZADATAK -----------------------------------------------
        IntStream sumaDuljine = bolesti.stream()
                .filter(bolest -> bolest instanceof Virus)
                .mapToInt( b-> b.getNaziv().length());
        System.out.println("Suma: " + sumaDuljine);

        Bolest min = bolesti.stream()
                .filter(bolest -> bolest instanceof Virus)
                .min(Comparator.comparing(Bolest::getNaziv))
                .get();
        System.out.println("Min: " + min);

        Bolest max = bolesti.stream()
                .filter(bolest -> bolest instanceof Virus)
                .max(Comparator.comparing(Bolest::getNaziv))
                .get();
        System.out.println("Max: " + max);

        double avg = bolesti.stream()
                .filter(bolest -> bolest instanceof Virus)
                .mapToInt( b-> b.getNaziv().length())
                .average()
                .getAsDouble();

        System.out.println("Avg: " + avg);



        ///TRECI ZADATAK-----------------------------------


        unos.close();
    }



    private static Zupanija unosZupanije(Scanner unos) {
        System.out.print("Unesite naziv županije: ");
        String nazivZupanije = unos.nextLine();
        int brojStanovnika = 0;
        int brojZarazenih = 0;
        boolean neispravanUnos = false;
        do {
            try {
                System.out.print("Unesite broj stanovnika: ");
                brojStanovnika = unos.nextInt();
                unos.nextLine();
                System.out.print("Unesite broj zaraženih stanovnika: ");
                brojZarazenih = unos.nextInt();
                unos.nextLine();
                neispravanUnos = false;
            }
            catch (InputMismatchException iznimka){
                System.out.println("Pogreška u formatu podataka, molimo ponovite unos!");
                logger.error("Pogreška u formatu podataka, molimo ponovite unos!", iznimka);
                unos.nextLine();
                neispravanUnos = true;
            }
        }while(neispravanUnos);
        return new Zupanija(nazivZupanije, brojStanovnika, brojZarazenih);
    }


    private static Simptom unosSimptoma(Scanner unos) {
        System.out.print("Unesite naziv simptoma: ");
        String nazivSimptoma = unos.nextLine();
        System.out.print("Unesite vrijednost simptoma (RIJETKO, SREDNJE ILI ČESTO): ");
        String vrijednostSimptoma = unos.nextLine();
        return new Simptom(nazivSimptoma, vrijednostSimptoma);
    }


    private static Bolest unosBolesti(Scanner unos, Set<Simptom> simptomi) {
        boolean neispravanUnos = false;
        int odabirBolesti = 0;
        String nazivBolesti = null;
        Simptom[] simptomiBolesti;
        System.out.println("Unosite li bolest ili virus: ");
        do {
            try {
                System.out.println("1) BOLEST ");
                System.out.println("2) VIRUS ");
                System.out.print("Odabir >> ");
                odabirBolesti = unos.nextInt();
                neispravanUnos = false;
            }catch (InputMismatchException iznimka){
                System.out.println("Pogreška u formatu podataka, molimo ponovite unos!");
                logger.error("Pogreška u formatu podataka, molimo ponovite unos!", iznimka);
                unos.nextLine();
                neispravanUnos = true;
            }
        }while (neispravanUnos);
        unos.nextLine();

        System.out.print("Unesite naziv bolesti ili virusa: ");
        nazivBolesti = unos.nextLine();
        System.out.print("Unesite broj simptoma: ");
        int brojSimptoma = unos.nextInt();
        unos.nextLine();
        simptomiBolesti = new Simptom[brojSimptoma];

        for (int j = 0; j < brojSimptoma; j++) {
            System.out.println("Odaberite " + (j + 1) + " simptom:");
            simptomiBolesti[j] = odabirSimptoma(unos, simptomi);
        }

        if(odabirBolesti == 1) {
            return new Bolest(nazivBolesti, simptomiBolesti);
        } else if(odabirBolesti == 2){
            return new Virus(nazivBolesti, simptomiBolesti);
        }
        return null;
    }


    private static Osoba unosOsobe(Scanner unos, Set<Bolest> bolesti, Set<Zupanija> zupanije, int i) {
        boolean neispravanUnos = false;
        Integer starost = null;

            System.out.print("Unesite ime " + (i + 1) + ". osobe: ");
            String ime = unos.nextLine();
            System.out.print("Unesite prezime osobe: ");
            String prezime = unos.nextLine();
            do {
                try {
                    System.out.print("Unesite starost osobe: ");
                    starost = unos.nextInt();
                    neispravanUnos = false;
                }
                catch (InputMismatchException iznimka){
                    System.out.println("Pogreška u formatu podataka, molimo ponovite unos!");
                    logger.error("Pogreška u formatu podataka, molimo ponovite unos!", iznimka);
                    unos.nextLine();
                    neispravanUnos = true;
                }
            }while (neispravanUnos);
            unos.nextLine();
            System.out.println("Unesite županiju prebivališta osobe:");
            Zupanija zupanijaOsobe = odabirZupanije(unos, zupanije);
            System.out.println("Unesite bolest osobe:");
            Bolest bolestOsobe = odabirBolesti(unos, bolesti);

            List<Osoba> kontakti = new ArrayList<>();

        return new Osoba.Builder(ime)
                .saPrezimenom(prezime)
                .starosti(starost)
                .izZupanije(zupanijaOsobe)
                .imaBolest(bolestOsobe)
                .saKontaktima(kontakti)
                .build();
    }


    private static Simptom odabirSimptoma(Scanner unos, Set<Simptom> simptomi) {
        int odabraniSimptom;
        int index = 0;
        Map<Integer, Simptom> mapaindexiranihSimptoma = new HashMap<>();
        do {
            int i = 0;
            for (Simptom s : simptomi){
                System.out.println(i+1 + " " + s.getNaziv() + " " + s.getVrijednost());
                index = i+1;
                mapaindexiranihSimptoma.put(index, s);
                i++;
            }
            System.out.print("Odabir: ");
            odabraniSimptom = unos.nextInt();
            if (odabraniSimptom > BROJ_SIMPTOMA) {
                System.out.println("Neispravan unos, molimo pokušajte ponovno! ");
            }
        } while (odabraniSimptom <= 0 || odabraniSimptom > BROJ_SIMPTOMA);
        unos.nextLine();

        return mapaindexiranihSimptoma.get(odabraniSimptom);
    }


    private static Bolest odabirBolesti(Scanner unos, Set<Bolest> bolesti) {
        Integer index = 0;
        Map<Integer, Bolest> mapaindexiranihBolesti = new HashMap<>();

        int i = 0;
        for (Bolest b : bolesti){
            System.out.println(i+1 + " " + b.getNaziv());
            index = i+1;
            mapaindexiranihBolesti.put(index, b);
            i++;
        }
        System.out.print("Odabir: ");
        int odabranaBolest = unos.nextInt();
        unos.nextLine();
        return mapaindexiranihBolesti.get(odabranaBolest);
    }


    private static Zupanija odabirZupanije(Scanner unos, Set<Zupanija> zupanije) {
        Integer index = 0;
        Map<Integer, Zupanija> mapaindexiranihZupanija = new HashMap<>();

        int i = 0;
        for (Zupanija z : zupanije){
            System.out.println(i+1 + " " + z.getNaziv());
            index = i+1;
            mapaindexiranihZupanija.put(index, z);
            i++;
        }
        System.out.print("Odabir: ");
        int odabranaZupanija = unos.nextInt();
        unos.nextLine();
        return (Zupanija) mapaindexiranihZupanija.get(odabranaZupanija);
    }


    private static void unosKontaktiranihOsoba(Scanner unos, List<Osoba> osobe, int i){
        int odabranaOsoba = 0;
        int brojKontaktiranihOsoba = 0;
        boolean neispravanUnos = false;
        boolean neispravan = false;
        do {
            try {
                System.out.print("Unesite broj osoba koje su bile u kontaktu s tom osobom: ");
                brojKontaktiranihOsoba = unos.nextInt();
                provjeraBrojaOsoba(brojKontaktiranihOsoba, i);
                neispravan = false;
            }catch (InputMismatchException iznimka){
                System.out.println("Neispravan unos, molimo ponovite unos!");
                logger.error("Pogreška u formatu podataka, molimo ponovite unos!", iznimka);
                unos.nextLine();
                neispravan = true;
            }catch (BrojKontaktiranihOsoba iznimka){
                System.out.println(iznimka.getMessage());
                logger.error(iznimka.getMessage());
                unos.nextLine();
                neispravan = true;
            }
        }while (neispravan);
        unos.nextLine();
        List<Osoba> tmp = new ArrayList<>(brojKontaktiranihOsoba);
        for(int j=0; j<brojKontaktiranihOsoba; j++) {
            System.out.println("Odaberite " + (j + 1) + " osobu:");
            do {
                try {
                    for (int k = 0; k < i; k++) {
                        System.out.println(k + 1 + "." + osobe.get(k).getIme() + " " + osobe.get(k).getPrezime());
                    }
                    System.out.print("Odabir: ");
                    odabranaOsoba = unos.nextInt();
                    provjeraKontaktiraneOsobe(osobe.get(odabranaOsoba - 1), osobe.get(j).getKontaktiraneOsobe());
                    neispravanUnos = false;
                }catch (DuplikatKontaktiraneOsobe iznimka){
                    System.out.println(iznimka.getMessage());
                    logger.error(iznimka.getMessage());
                    neispravanUnos = true;
                }
                unos.nextLine();
            }while (neispravanUnos);
            tmp.add(osobe.get(odabranaOsoba - 1));
        }
        osobe.get(i).setKontaktiraneOsobe(tmp);

        for(int j=0;j<brojKontaktiranihOsoba; j++){
            if(tmp.get(j).getZarazenBolescu() instanceof Virus virus){
                virus.prelazakZarazeNaOsobu(osobe.get(i));
            }
        }
    }

    private static void provjeraBrojaOsoba(int brojKontaktiranihOsoba, int i) throws BrojKontaktiranihOsoba {
        if(brojKontaktiranihOsoba>=i+1 || brojKontaktiranihOsoba<0){
            throw new BrojKontaktiranihOsoba("Uneseni broj osoba ne odgovara!");
        }
    }

    private static void provjeraKontaktiraneOsobe(Osoba osoba, List<Osoba> kontaktiraneOsobe) throws DuplikatKontaktiraneOsobe {
        for (Osoba value : kontaktiraneOsobe) {
            if (osoba.equals(value)) {
                throw new DuplikatKontaktiraneOsobe("Odabrana osoba se već nalazi među kontaktiranim osobama. Molimo Vas da odaberete neku drugu osobu.");
            }
        }
    }

    private static void provjeraIsteBolesti(Set<Bolest> bolesti, Bolest tmp) {
        int velicinaPolja = bolesti.size();
        int i = 0;
        for(Bolest value : bolesti){
            if(i<velicinaPolja-2){
                if (Arrays.equals(tmp.getSimptomi(), value.getSimptomi())) {
                    throw new BolestIstihSimptoma("Pogrešan unos, već ste unijeli bolest ili virus s istim simptomima. Molimo ponovite unos.");
                }
                else break;
            }i++;
        }
    }



    private static void ispisOsoba(List<Osoba> osobe) {
        System.out.println("Popis osoba: ");
        for(int i = 0; i< BROJ_OSOBA; i++){
            System.out.println("Ime i prezime: "+ osobe.get(i).getIme() + " " + osobe.get(i).getPrezime());
            System.out.println("Starost: " + osobe.get(i).getStarost());
            System.out.println("Županija prebivališta: " + osobe.get(i).getZupanija().getNaziv());
            System.out.println("Zaražen bolešću: " + osobe.get(i).getZarazenBolescu().getNaziv());
            System.out.println("Kontaktirane osobe:");
            if(osobe.get(i).getKontaktiraneOsobe().size() > 0) {
                for (int j = 0; j < osobe.get(i).getKontaktiraneOsobe().size(); j++) {
                    System.out.println(osobe.get(i).getKontaktiraneOsobe().get(j).getIme() + " " + osobe.get(i).getKontaktiraneOsobe().get(j).getPrezime());
                }
            }else {
                System.out.println("Nema kontaktiranih osoba!");
            }
        }
    }
}