package hr.java.covidportal.genericsi;

import hr.java.covidportal.model.Virus;

public class KlasaZaViruse<T extends Virus> {
    
    T virus;
    
    public void add( T virus){
        this.virus = virus;
    }
    
    public KlasaZaViruse<T> getVirus(){
        return this;
    }
}
