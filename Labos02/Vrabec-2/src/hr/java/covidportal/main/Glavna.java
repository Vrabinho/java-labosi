package hr.java.covidportal.main;

import hr.java.covidportal.model.*;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Glavna {

    static final int BROJ_ZUPANIJA = 3;
    static final int BROJ_OSOBA = 3;
    static final int BROJ_SIMPTOMA = 3;
    static final int BROJ_BOLESTI = 4;


    public static void main(String[] args) {

        // klasa za unos podataka sa tipkovnice
        Scanner unos = new Scanner(System.in);

        //petlja za unos zupanija
        Zupanija[] zupanije = new Zupanija[BROJ_ZUPANIJA];
        System.out.println("Unesite podatke o " + BROJ_ZUPANIJA + " županije: ");

        //////PRVI ZADATAK---------------------------------------------------------------------------------------------------------
        boolean krivoUnesenaZupanija = false;
        int brojac = 0;
        do {
                Zupanija tmp;
                tmp = unosZupanije(unos, krivoUnesenaZupanija);
                if(tmp.getNaziv().isEmpty()){
                    krivoUnesenaZupanija=true;
                    break;
                }
                zupanije[brojac] = tmp;
                unos.nextLine();
            brojac++;
        }while(!krivoUnesenaZupanija);

        /////////DRUGI ZADATAK I TRECI ZADATAK------------------------------------------------------------------------------
        List<Zupanija> listaZupanija = Arrays.asList(zupanije);
        listaZupanija.stream()
                .sorted(Comparator.comparing(Zupanija::getNaziv).thenComparing(Zupanija::getBrojStanovnika))
                .forEach(System.out::println);

        //petlja za unos simptoma
        Simptom[] simptomi = new Simptom[BROJ_SIMPTOMA];
        System.out.println("Unesite podatke o " + BROJ_SIMPTOMA + " simptoma");

        for (int i = 0; i < BROJ_SIMPTOMA; i++) {
            simptomi[i] = unosSimptoma(unos);
        }

        //petlja za unos simptoma
        Bolest[] bolesti = new Bolest[BROJ_BOLESTI];
        System.out.println("Unesite podatke o " + BROJ_BOLESTI + " bolesti:");
        for (int i = 0; i < BROJ_BOLESTI; i++) {
            bolesti[i] = unosBolesti(unos, simptomi);
        }

        //petlja za unos osoba
        Osoba[] osobe = new Osoba[BROJ_OSOBA];
        for (int i = 0; i < BROJ_OSOBA; i++) {
            osobe[i] = unosOsobe(unos, bolesti, zupanije, i);
            if (i > 0) {
                unosKontaktiranihOsoba(unos, osobe, i);
            }
        }
        ispisOsoba(osobe);
        unos.close();
    }


    //UNOSI
    //unos zupanije - metoda
    private static Zupanija unosZupanije(Scanner unos, boolean krivoUnesenaZupanija) {
        System.out.print("Unesite naziv županije: ");
        String nazivZupanije = unos.nextLine();
        ///////PROSIRENJE PRVOG ZADATKA---------------------------------------------------------
        if(nazivZupanije.equals("\n") || nazivZupanije.isEmpty()) {
            krivoUnesenaZupanija = true;
        }
        System.out.print("Unesite broj stanovnika: ");
        Integer brojStanovnika = unos.nextInt();
        return new Zupanija(nazivZupanije, brojStanovnika);
    }

    //unos simptoma - metoda
    private static Simptom unosSimptoma(Scanner unos) {
        System.out.print("Unesite naziv simptoma: ");
        String nazivSimptoma = unos.nextLine();
        System.out.print("Unesite vrijednost simptoma (RIJETKO, SREDNJE ILI ČESTO): ");
        String vrijednostSimptoma = unos.nextLine();
        return new Simptom(nazivSimptoma, vrijednostSimptoma);
    }

    //Unos bolesti - metoda
    private static Bolest unosBolesti(Scanner unos, Simptom[] simptomi) {

        System.out.println("Unosite li bolest ili virus: ");
        System.out.println("1) BOLEST ");
        System.out.println("2) VIRUS ");
        System.out.print("Odabir >> ");
        int odabirBolesti = unos.nextInt();
        unos.nextLine();

        System.out.print("Unesite naziv bolesti ili virusa: ");
        String nazivBolesti = unos.nextLine();

        System.out.print("Unesite broj simptoma: ");
        int brojSimptoma = unos.nextInt();
        unos.nextLine();
        Simptom[] simptomiBolesti = new Simptom[brojSimptoma];
        for (int j = 0; j < brojSimptoma; j++) {
            System.out.println("Odaberite " + (j + 1) + " simptom:");
            simptomiBolesti[j] = odabirSimptoma(unos, simptomi);
        }

        if(odabirBolesti == 1) {
            // Bolest
            return new Bolest(nazivBolesti, simptomiBolesti);
        } else if(odabirBolesti == 2){
            // Virus
            return new Virus(nazivBolesti, simptomiBolesti);
        }
        return null;
    }

    //Unos Osoba - metoda
    private static Osoba unosOsobe(Scanner unos, Bolest[] bolesti, Zupanija[] zupanije, int i) {

            System.out.print("Unesite ime " + (i + 1) + ". osobe: ");
            String ime = unos.nextLine();
            System.out.print("Unesite prezime osobe: ");
            String prezime = unos.nextLine();
            System.out.print("Unesite starost osobe: ");
            Integer starost = unos.nextInt();
            unos.nextLine();
            System.out.println("Unesite županiju prebivališta osobe:");
            Zupanija zupanijaOsobe = odabirZupanije(unos, zupanije);
            System.out.println("Unesite bolest osobe:");
            Bolest bolestOsobe = odabirBolesti(unos, bolesti);

            Osoba[] kontakti = new Osoba[0];

        return new Osoba.Builder(ime)
                .saPrezimenom(prezime)
                .starosti(starost)
                .izZupanije(zupanijaOsobe)
                .imaBolest(bolestOsobe)
                .saKontaktima(kontakti)
                .build();
    }

    //ODABIRI

    //odabir simptoma - metoda
    private static Simptom odabirSimptoma(Scanner unos, Simptom[] simptomi) {
        int odabraniSimptom;
        do {
            for (int k = 0; k < BROJ_SIMPTOMA; k++) {
                System.out.println(k + 1 + ". " + simptomi[k].getNaziv() + " " + simptomi[k].getVrijednost());
            }
            System.out.print("Odabir: ");
            odabraniSimptom = unos.nextInt();
            if (odabraniSimptom > BROJ_SIMPTOMA) {
                System.out.println("Neispravan unos, molimo pokušajte ponovno! ");
            }
        } while (odabraniSimptom <= 0 || odabraniSimptom > BROJ_SIMPTOMA);
        unos.nextLine();
        return simptomi[odabraniSimptom - 1];
    }

    //Odabir bolesti
    private static Bolest odabirBolesti(Scanner unos, Bolest[] bolesti) {
        for (int i = 0; i < BROJ_BOLESTI; i++) {
            System.out.println(i + 1 + ". " + bolesti[i].getNaziv());
        }
        System.out.print("Odabir: ");
        int odabranaBolest = unos.nextInt();
        unos.nextLine();
        return bolesti[odabranaBolest - 1];
    }

    //Odabir zupanije
    private static Zupanija odabirZupanije(Scanner unos, Zupanija[] zupanije) {
        for (int i = 0; i < BROJ_ZUPANIJA; i++) {
            System.out.println(i + 1 + ". " + zupanije[i].getNaziv());
        }
        System.out.print("Odabir: ");
        int odabranaZupanija = unos.nextInt();
        unos.nextLine();
        return zupanije[odabranaZupanija - 1];
    }

    //Odabir kontaktirane osobe
    private static void unosKontaktiranihOsoba(Scanner unos, Osoba[] osobe, int i) {
        System.out.println("Unesite broj osoba koje su bile u kontaktu s tom osobom: ");
        int brojKontaktiranihOsoba = unos.nextInt();
        unos.nextLine();
        Osoba[] tmp = new Osoba[brojKontaktiranihOsoba];
        for(int j=0; j<brojKontaktiranihOsoba; j++){
            System.out.println("Odaberite " + (j + 1) + " osobu:");
            for (int k = 0; k < i; k++) {
                System.out.println(k + 1 + "." + osobe[k].getIme() + " " + osobe[k].getPrezime());
            }
            System.out.print("Odabir: ");
            int odabranaOsoba = unos.nextInt();
            unos.nextLine();
            tmp[j] = osobe[odabranaOsoba-1];
        }
        osobe[i].setKontaktiraneOsobe(tmp);
        //provjera da li je virus
        for(int j=0;j<brojKontaktiranihOsoba; j++){
            if(tmp[j].getZarazenBolescu() instanceof Virus virus){
                virus.prelazakZarazeNaOsobu(osobe[i]);
            }
        }
    }


    //ISPIS
    private static void ispisOsoba(Osoba[] osobe) {
        System.out.println("Popis osoba: ");
        for(int i = 0; i< BROJ_OSOBA; i++){
            System.out.println("Ime i prezime: "+ osobe[i].getIme() + " " + osobe[i].getPrezime());
            System.out.println("Starost: " + osobe[i].getStarost());
            System.out.println("Županija prebivališta: " + osobe[i].getZupanija().getNaziv());
            System.out.println("Zaražen bolešću: " + osobe[i].getZarazenBolescu().getNaziv());
            System.out.println("Kontaktirane osobe:");
            if(osobe[i].getKontaktiraneOsobe().length > 0) {
                for (int j = 0; j < osobe[i].getKontaktiraneOsobe().length; j++) {
                    System.out.println(osobe[i].getKontaktiraneOsobe()[j].getIme() + " " + osobe[i].getKontaktiraneOsobe()[j].getPrezime());
                }
            }else {
                System.out.println("Nema kontaktiranih osoba!");
            }
        }
    }
}
