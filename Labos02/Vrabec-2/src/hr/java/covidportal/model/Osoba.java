package hr.java.covidportal.model;

public class Osoba {
    String ime;
    String prezime;
    Integer starost;
    Zupanija zupanija;
    Bolest zarazenBolescu;
    Osoba[] kontaktiraneOsobe;

    public static class Builder{
        String ime;
        String prezime;
        Integer starost;
        Zupanija zupanija;
        Bolest zarazenBolescu;
        Osoba[] kontaktiraneOsobe;

        // Builder konstruktor
        public Builder(String ime){
            this.ime = ime;
        }

        public Builder saPrezimenom(String prezime){
            this.prezime = prezime;
            return this;
        }

        public Builder starosti(Integer godine){
            this.starost = godine;
            return this;
        }

        public Builder izZupanije(Zupanija zupanija){
            this.zupanija = zupanija;
            return this;
        }

        public Builder imaBolest(Bolest bol){
            this.zarazenBolescu = bol;
            return this;
        }

        public Builder saKontaktima(Osoba[] kontakti){
            this.kontaktiraneOsobe = kontakti;
            return this;
        }

        public Osoba build() {
            Osoba osoba = new Osoba();
            osoba.ime = this.ime;
            osoba.prezime = this.prezime;
            osoba.starost = this.starost;
            osoba.zupanija = this.zupanija;
            osoba.kontaktiraneOsobe = this.kontaktiraneOsobe;
            osoba.zarazenBolescu = this.zarazenBolescu;
            return osoba;
        }
    }

    public Osoba() {}

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public Integer getStarost() {
        return starost;
    }

    public void setStarost(Integer starost) {
        this.starost = starost;
    }

    public Zupanija getZupanija() {
        return zupanija;
    }

    public void setZupanija(Zupanija zupanija) {
        this.zupanija = zupanija;
    }

    public Bolest getZarazenBolescu() {
        return zarazenBolescu;
    }

    public void setZarazenBolescu(Bolest zarazenBolescu) {
        this.zarazenBolescu = zarazenBolescu;
    }

    public Osoba[] getKontaktiraneOsobe() {
        return kontaktiraneOsobe;
    }

    public void setKontaktiraneOsobe(Osoba[] kontaktiraneOsobe) {
        this.kontaktiraneOsobe = kontaktiraneOsobe;
    }
}
