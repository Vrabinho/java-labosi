package hr.java.covidportal.main;

import hr.java.covidportal.Iznimke.*;
import hr.java.covidportal.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Glavna {

    static final int BROJ_ZUPANIJA = 3;
    static final int BROJ_OSOBA = 3;
    static final int BROJ_SIMPTOMA = 3;
    static final int BROJ_BOLESTI = 4;

    private static final Logger logger = LoggerFactory.getLogger(Glavna.class);


    public static void main(String[] args){


        Scanner unos = new Scanner(System.in);


        Zupanija[] zupanije = new Zupanija[BROJ_ZUPANIJA];
        System.out.println("Unesite podatke o " + BROJ_ZUPANIJA + " županije: ");

        for (int i = 0; i < BROJ_ZUPANIJA; i++) {
            zupanije[i] = unosZupanije(unos);
            unos.nextLine();
        }


        Simptom[] simptomi = new Simptom[BROJ_SIMPTOMA];
        System.out.println("Unesite podatke o " + BROJ_SIMPTOMA + " simptoma");

        for (int i = 0; i < BROJ_SIMPTOMA; i++) {
            simptomi[i] = unosSimptoma(unos);
        }


        Bolest[] bolesti = new Bolest[BROJ_BOLESTI];
        System.out.println("Unesite podatke o " + BROJ_BOLESTI + " bolesti:");
        boolean neispravnaBolest = false;
        for (int i = 0; i < BROJ_BOLESTI; i++) {
            do{
                neispravnaBolest = false;
                    bolesti[i] = unosBolesti(unos, simptomi);
                    Bolest tmp = bolesti[i];
                    if(i>0){
                    try{
                        //provjeraIsteBolesti(bolesti, tmp, i);
                        /////////PROVJERAVANJE NAZIVA VIRUSA--------------------------------------------------------
                        /**
                         * Provjera ako smo unijeli dva ili vise puta isti virus
                         */
                        provjeraDuplogVirusa(bolesti, tmp);
                        neispravnaBolest = false;
                    }catch (DupliVirusException iznimka) {
                        System.out.println(iznimka.getMessage());
                        logger.error(iznimka.getMessage());
                        neispravnaBolest = true;
                        i--;
                    }
                }
            }while (neispravnaBolest);
        }

        Osoba[] osobe = new Osoba[BROJ_OSOBA];
        for (int i = 0; i < BROJ_OSOBA; i++) {
            osobe[i] = unosOsobe(unos, bolesti, zupanije, i);
            if (i > 0) {
                unosKontaktiranihOsoba(unos, osobe, i);
            }
        }
        ispisOsoba(osobe);
        unos.close();
    }



    private static Zupanija unosZupanije(Scanner unos) {
        System.out.print("Unesite naziv županije: ");
        String nazivZupanije = unos.nextLine();
        int brojStanovnika = 0;
        boolean neispravanUnos = false;
        do {
            try {
                System.out.print("Unesite broj stanovnika: ");
                brojStanovnika = unos.nextInt();
                neispravanUnos = false;
            }
            catch (InputMismatchException iznimka){
                System.out.println("Pogreška u formatu podataka, molimo ponovite unos!");
                logger.error("Pogreška u formatu podataka, molimo ponovite unos!", iznimka);
                unos.nextLine();
                neispravanUnos = true;
            }
        }while(neispravanUnos);
        return new Zupanija(nazivZupanije, brojStanovnika);
    }


    private static Simptom unosSimptoma(Scanner unos) {
        System.out.print("Unesite naziv simptoma: ");
        String nazivSimptoma = unos.nextLine();
        System.out.print("Unesite vrijednost simptoma (RIJETKO, SREDNJE ILI ČESTO): ");
        String vrijednostSimptoma = unos.nextLine();
        return new Simptom(nazivSimptoma, vrijednostSimptoma);
    }


    private static Bolest unosBolesti(Scanner unos, Simptom[] simptomi) {
        boolean neispravanUnos = false;
        int odabirBolesti = 0;
        String nazivBolesti = null;
        Simptom[] simptomiBolesti = new Simptom[0];
        System.out.println("Unosite li bolest ili virus: ");
        do {
            try {
                System.out.println("1) BOLEST ");
                System.out.println("2) VIRUS ");
                System.out.print("Odabir >> ");
                odabirBolesti = unos.nextInt();
                neispravanUnos = false;
            }catch (InputMismatchException iznimka){
                System.out.println("Pogreška u formatu podataka, molimo ponovite unos!");
                logger.error("Pogreška u formatu podataka, molimo ponovite unos!", iznimka);
                unos.nextLine();
                neispravanUnos = true;
            }
        }while (neispravanUnos);
        unos.nextLine();

        ///regex provjera za virus---------------------------------------------------------------
        boolean neispravanVirus = false;
        String regex = "^[A-Za-z]{15}$";

        Pattern pattern = Pattern.compile(regex);

        do{
            try {
                System.out.print("Unesite naziv bolesti ili virusa: ");
                nazivBolesti = unos.nextLine();
                Matcher matcher = pattern.matcher(nazivBolesti);
                provjeraVirusa(matcher, odabirBolesti);
                neispravanVirus = false;

            } catch (NeispravanNazivException iznimka) {
                System.out.println(iznimka.getMessage());
                logger.error(iznimka.getMessage());
                neispravanVirus = true;
            }
        }while(neispravanVirus);

        System.out.print("Unesite broj simptoma: ");
        int brojSimptoma = unos.nextInt();
        unos.nextLine();
        simptomiBolesti = new Simptom[brojSimptoma];

        for (int j = 0; j < brojSimptoma; j++) {
            System.out.println("Odaberite " + (j + 1) + " simptom:");
            simptomiBolesti[j] = odabirSimptoma(unos, simptomi);
        }

        if(odabirBolesti == 1) {
            return new Bolest(nazivBolesti, simptomiBolesti);
        } else if(odabirBolesti == 2){

            return new Virus(nazivBolesti, simptomiBolesti);
        }
        return null;
    }



    private static Osoba unosOsobe(Scanner unos, Bolest[] bolesti, Zupanija[] zupanije, int i) {
        boolean neispravanUnos = false;
        Integer starost = null;

            System.out.print("Unesite ime " + (i + 1) + ". osobe: ");
            String ime = unos.nextLine();
            System.out.print("Unesite prezime osobe: ");
            String prezime = unos.nextLine();
            do {
                try {
                    System.out.print("Unesite starost osobe: ");
                    starost = unos.nextInt();
                    neispravanUnos = false;
                }
                catch (InputMismatchException iznimka){
                    System.out.println("Pogreška u formatu podataka, molimo ponovite unos!");
                    logger.error("Pogreška u formatu podataka, molimo ponovite unos!", iznimka);
                    unos.nextLine();
                    neispravanUnos = true;
                }
            }while (neispravanUnos);
            unos.nextLine();
            System.out.println("Unesite županiju prebivališta osobe:");
            Zupanija zupanijaOsobe = odabirZupanije(unos, zupanije);
            System.out.println("Unesite bolest osobe:");
            Bolest bolestOsobe = odabirBolesti(unos, bolesti);

            Osoba[] kontakti = new Osoba[0];

        return new Osoba.Builder(ime)
                .saPrezimenom(prezime)
                .starosti(starost)
                .izZupanije(zupanijaOsobe)
                .imaBolest(bolestOsobe)
                .saKontaktima(kontakti)
                .build();
    }


    private static Simptom odabirSimptoma(Scanner unos, Simptom[] simptomi) {
        int odabraniSimptom;
        do {
            for (int k = 0; k < BROJ_SIMPTOMA; k++) {
                System.out.println(k + 1 + ". " + simptomi[k].getNaziv() + " " + simptomi[k].getVrijednost());
            }
            System.out.print("Odabir: ");
            odabraniSimptom = unos.nextInt();
            if (odabraniSimptom > BROJ_SIMPTOMA) {
                System.out.println("Neispravan unos, molimo pokušajte ponovno! ");
            }
        } while (odabraniSimptom <= 0 || odabraniSimptom > BROJ_SIMPTOMA);
        unos.nextLine();
        return simptomi[odabraniSimptom - 1];
    }


    private static Bolest odabirBolesti(Scanner unos, Bolest[] bolesti) {
        for (int i = 0; i < BROJ_BOLESTI; i++) {
            System.out.println(i + 1 + ". " + bolesti[i].getNaziv());
        }
        System.out.print("Odabir: ");
        int odabranaBolest = unos.nextInt();
        unos.nextLine();
        return bolesti[odabranaBolest - 1];
    }


    private static Zupanija odabirZupanije(Scanner unos, Zupanija[] zupanije) {
        for (int i = 0; i < BROJ_ZUPANIJA; i++) {
            System.out.println(i + 1 + ". " + zupanije[i].getNaziv());
        }
        System.out.print("Odabir: ");
        int odabranaZupanija = unos.nextInt();
        unos.nextLine();
        return zupanije[odabranaZupanija - 1];
    }


    private static void unosKontaktiranihOsoba(Scanner unos, Osoba[] osobe, int i){
        int odabranaOsoba = 0;
        int brojKontaktiranihOsoba = 0;
        boolean neispravanUnos = false;
        boolean neispravan = false;
        do {
            try {
                System.out.println("Unesite broj osoba koje su bile u kontaktu s tom osobom: ");
                brojKontaktiranihOsoba = unos.nextInt();
                provjeraBrojaOsoba(brojKontaktiranihOsoba, i);
                neispravan = false;
            }catch (InputMismatchException iznimka){
                System.out.println("Neispravan unos, molimo ponovite unos!");
                logger.error("Pogreška u formatu podataka, molimo ponovite unos!", iznimka);
                unos.nextLine();
                neispravan = true;
            }catch (BrojKontaktiranihOsoba iznimka){
                System.out.println(iznimka.getMessage());
                logger.error(iznimka.getMessage());
                unos.nextLine();
                neispravan = true;
            }
        }while (neispravan);
        unos.nextLine();
        Osoba[] tmp = new Osoba[brojKontaktiranihOsoba];
        for(int j=0; j<brojKontaktiranihOsoba; j++) {
            System.out.println("Odaberite " + (j + 1) + " osobu:");
            do {
                try {
                    for (int k = 0; k < i; k++) {
                        System.out.println(k + 1 + "." + osobe[k].getIme() + " " + osobe[k].getPrezime());
                    }
                    System.out.print("Odabir: ");
                    odabranaOsoba = unos.nextInt();
                    provjeraKontaktiraneOsobe(osobe[odabranaOsoba-1], osobe[j].getKontaktiraneOsobe());
                    neispravanUnos = false;
                }catch (DuplikatKontaktiraneOsobe iznimka){
                    System.out.println(iznimka.getMessage());
                    logger.error(iznimka.getMessage());
                    neispravanUnos = true;
                }
                unos.nextLine();
            }while (neispravanUnos);
            tmp[j] = osobe[odabranaOsoba-1];
        }
        osobe[i].setKontaktiraneOsobe(tmp);

        for(int j=0;j<brojKontaktiranihOsoba; j++){
            if(tmp[j].getZarazenBolescu() instanceof Virus virus){
                virus.prelazakZarazeNaOsobu(osobe[i]);
            }
        }
    }

    private static void provjeraBrojaOsoba(int brojKontaktiranihOsoba, int i) throws BrojKontaktiranihOsoba {
        if(brojKontaktiranihOsoba>=i+1 || brojKontaktiranihOsoba<0){
            throw new BrojKontaktiranihOsoba("Uneseni broj osoba ne odgovara!");
        }
    }

    private static void provjeraKontaktiraneOsobe(Osoba osoba, Osoba[] kontaktiraneOsobe) throws DuplikatKontaktiraneOsobe {
        for (Osoba value : kontaktiraneOsobe) {
            if (osoba.equals(value)) {
                throw new DuplikatKontaktiraneOsobe("Odabrana osoba se već nalazi među kontaktiranim osobama. Molimo Vas da odaberete neku drugu osobu.");
            }
        }
    }

    /**
     * provjera naziva virusa
     * @param matcher matcher koji usporeduje da li je false ili true
     * @param odabirBolesti u ovome parametru saljemo ako je korisnik odabrao virus
     * @throws NeispravanNazivException
     */
    private static void provjeraVirusa(Matcher matcher, int odabirBolesti) throws NeispravanNazivException {
        if(matcher.matches() && odabirBolesti == 2){
            throw new NeispravanNazivException("Neispravan naziv virusa. Molimo unesite ponovo naziv.");
        }
    }


    /**
     * Provjeravanje unsesenog istog naziva virusa
     * @param bolesti u petlji prolazimo po polju unesenim bolestima
     * @param bolest provjera bolesti sa ostalim bolestima
     * @throws DupliVirusException ako je true, baca klasu <code>DupliVirusException</code>
     */
    private static void provjeraDuplogVirusa(Bolest[] bolesti, Bolest bolest) throws DupliVirusException {
        for (Bolest value : bolesti) {
            if (bolest.getNaziv().equals(value.getNaziv()) && bolest instanceof Virus virus) {
                throw new DupliVirusException("Isti naziv virusa, unesite ponovo virus.");
            }
        }
    }



/*
    private static void provjeraIsteBolesti(Bolest[] bolesti, Bolest tmp, int brojac) throws BolestIstihSimptoma {
        for(int j=0;j<brojac;j++){
            if(tmp.getSimptomi().equals(bolesti[j].getSimptomi())){
                throw new BolestIstihSimptoma("Pogrešan unos, već ste unijeli bolest ili virus s istim simptomima. Molimo ponovite unos.");

            }
        }
    }
*/


    private static void ispisOsoba(Osoba[] osobe) {
        System.out.println("Popis osoba: ");
        for(int i = 0; i< BROJ_OSOBA; i++){
            System.out.println("Ime i prezime: "+ osobe[i].getIme() + " " + osobe[i].getPrezime());
            System.out.println("Starost: " + osobe[i].getStarost());
            System.out.println("Županija prebivališta: " + osobe[i].getZupanija().getNaziv());
            System.out.println("Zaražen bolešću: " + osobe[i].getZarazenBolescu().getNaziv());
            System.out.println("Kontaktirane osobe:");
            if(osobe[i].getKontaktiraneOsobe().length > 0) {
                for (int j = 0; j < osobe[i].getKontaktiraneOsobe().length; j++) {
                    System.out.println(osobe[i].getKontaktiraneOsobe()[j].getIme() + " " + osobe[i].getKontaktiraneOsobe()[j].getPrezime());
                }
            }else {
                System.out.println("Nema kontaktiranih osoba!");
            }
        }
    }
}
