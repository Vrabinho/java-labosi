package hr.java.covidportal.model;

/**
 * prima polje simptoma
 */

public class Bolest extends ImenovaniEntitet  {


    Simptom[] simptomi;

    /**
     * konstrujtor sa dva parametra
     * @param naziv prima naziv bolesti koji saljemo u nadklasu
     * @param simptomi prima polje simptoma
     */

    public Bolest(String naziv, Simptom[] simptomi) {
        super(naziv);
        this.simptomi = simptomi;
    }


    public Simptom[] getSimptomi() {
        return simptomi;
    }

    public void setSimptomi(Simptom[] simptomi) {
        this.simptomi = simptomi;
    }
}
