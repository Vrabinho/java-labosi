package hr.java.covidportal.model;

/**
 * Sprema simptom i extenda <code>ImenaovaniEntitet</code>
 */
public class Simptom extends ImenovaniEntitet{


    String vrijednost;

    /**
     * konstruktor sa jednim parametrom
     * @param naziv prima naziv i salje ga u u konstruktor nadklase <code>ImenaovaniEntitet</code>
     * @param vrijednost prima string
     */
    public Simptom(String naziv, String vrijednost) {
        super(naziv);
        this.vrijednost = vrijednost;
    }


    public String getVrijednost() {
        return vrijednost;
    }

    public void setVrijednost(String vrijednost) {
        this.vrijednost = vrijednost;
    }
}
