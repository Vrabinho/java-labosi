package hr.java.covidportal.model;

/**
 * Nadklasa sa parametrom naziv preko koje unosimo nazive za sve objekte
 * koji sadrze parametar naziv
 */

public abstract class ImenovaniEntitet {
    String naziv;

    /**
     * konstruktor sa jednim parametrom
     * @param naziv prima naziv objekta
     */
    public ImenovaniEntitet(String naziv) {
        this.naziv = naziv;
    }

    public ImenovaniEntitet() {
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }
}
