package hr.java.covidportal.model;

/**
 * Extenda klasu <code>Bolest</code> i implenetira interface <code>Zarazno</code>
 */
public class Virus extends Bolest implements Zarazno {

    /**
     * konstruktor sa dva parametra
     * @param naziv prima naziv i salje ga u nadklasu <code>Bolest</code>
     * @param simptomi prima polje simptoma i salje ga u nadklasu <code>Bolest</code>
     */
    public Virus(String naziv, Simptom[] simptomi) {
        super(naziv, simptomi);
    }


    /**
     * Prenosi zarazu na kontaktirane osobe
     * Kao parametar prima objekt <code>Osoba</code>
     * @param osoba prima objekt <code>Osoba</code>
     */
    @Override
    public void prelazakZarazeNaOsobu(Osoba osoba) {
        osoba.setZarazenBolescu(this);
    }
}
