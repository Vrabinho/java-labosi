package hr.java.covidportal.model;

/**
 * Extenda <code>ImenovaniEntitet</code>
 */

public class Zupanija extends ImenovaniEntitet{


    Integer brojStanovnika;

    /**
     * Konstruktor sa dva parametra
     * @param naziv prima naziv zupanije i proslijeduje ga nadklasi <code>ImenovaniEntitet</code>
     * @param brojStanovnika prima integer
     */

    public Zupanija(String naziv, Integer brojStanovnika) {
        super(naziv);
        this.brojStanovnika = brojStanovnika;
    }


    public Integer getBrojStanovnika() {
        return brojStanovnika;
    }

    public void setBrojStanovnika(Integer brojStanovnika) {
        this.brojStanovnika = brojStanovnika;
    }
}
