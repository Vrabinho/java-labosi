package hr.java.covidportal.Iznimke;

/**
 * Provjeravanje naziva Virusa
 */

public class NeispravanNazivException extends Exception{

    /**
     * konstruktor sa jednim parametrom
     * @param message prima poruku iznimke
     */
    public NeispravanNazivException(String message) {
        super(message);
    }

    /**
     * konstruktor sa dva parametra
     * @param message prima poruku iznimke
     * @param cause prima uzrok iznimke
     */
    public NeispravanNazivException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * konstruktor sa jednim parametrom
     * @param cause prima uzrok iznimke
     */
    public NeispravanNazivException(Throwable cause) {
        super(cause);
    }
}
