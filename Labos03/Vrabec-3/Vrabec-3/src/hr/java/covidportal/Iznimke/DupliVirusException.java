package hr.java.covidportal.Iznimke;

/**
 * Provjera  unosa istog virusa
 */

public class DupliVirusException extends RuntimeException{

    /**
     * konstruktor sa jednim parametrom koji vraca poruku
     * @param message prima string
     */
    public DupliVirusException(String message) {
        super(message);
    }

    /**
     * konstruktor sa dva parametra koji vraca poruku i uzrok iznimke
     * @param message poruka iznimke
     * @param cause uzrok iznimke
     */
    public DupliVirusException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * konstruktor sa jednim parametrom koji vraca i prima uzrok iznimke
     * @param cause prima uzrok iznimke
     */
    public DupliVirusException(Throwable cause) {
        super(cause);
    }
}
