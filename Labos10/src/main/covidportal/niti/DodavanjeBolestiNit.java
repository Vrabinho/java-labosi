package main.covidportal.niti;

import main.covidportal.baza.BazaPodataka;
import main.covidportal.model.Bolest;

import java.io.IOException;
import java.sql.SQLException;

public class DodavanjeBolestiNit implements Runnable {

    Bolest b;

    public DodavanjeBolestiNit(Bolest b) {
        this.b = b;
    }

    private synchronized void dodajbolest() {

        while (BazaPodataka.aktivnaVezaSBazomPodataka) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        try {
            BazaPodataka.spremiNovuBolest(this.b);
        } catch (SQLException | IOException throwables) {
            throwables.printStackTrace();
        }
        notifyAll();
    }

    @Override
    public void run() {
        dodajbolest();
    }
}
