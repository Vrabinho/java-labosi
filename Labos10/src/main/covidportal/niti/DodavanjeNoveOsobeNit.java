package main.covidportal.niti;

import main.covidportal.baza.BazaPodataka;
import main.covidportal.model.Osoba;

import java.io.IOException;
import java.sql.SQLException;

public class DodavanjeNoveOsobeNit implements Runnable {

    Osoba o;

    public DodavanjeNoveOsobeNit(Osoba o) {
        this.o = o;
    }

    private synchronized void dodajOsobu() {

        while (BazaPodataka.aktivnaVezaSBazomPodataka) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        try {
            BazaPodataka.spremiNovuOsobu(this.o);
        } catch (SQLException | IOException throwables) {
            throwables.printStackTrace();
        }
        notifyAll();
    }

    @Override
    public void run() {
        dodajOsobu();
    }
}
