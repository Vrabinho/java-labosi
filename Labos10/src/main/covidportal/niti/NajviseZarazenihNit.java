package main.covidportal.niti;

import main.covidportal.baza.BazaPodataka;
import main.covidportal.model.Zupanija;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public class NajviseZarazenihNit implements Runnable {

    Zupanija z;

    public NajviseZarazenihNit() {
    }

    public NajviseZarazenihNit(Zupanija z) {
        this.z = z;
    }

    private synchronized Zupanija najzarazenija() {

        while (BazaPodataka.aktivnaVezaSBazomPodataka) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        List<Zupanija> listZupanija = null;
        try {
            listZupanija = BazaPodataka.dohvatiSveZupanijeIzBaze();
        } catch (SQLException | IOException throwables) {
            throwables.printStackTrace();
        }

        Zupanija maxZup = listZupanija.stream()
                .max(Comparator.comparing(Zupanija::getPostotakZarazenih))
                .get();

        notifyAll();
        return maxZup;
    }

    @Override
    public void run() {

        while (true) {
            try {
                Zupanija z = najzarazenija();
                System.out.println("Županija s najviše zaraženih stanovnika je " + z.getNaziv() + " i to " + z.getPostotakZarazenih() + "%.");
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}