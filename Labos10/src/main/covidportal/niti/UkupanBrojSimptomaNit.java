package main.covidportal.niti;

import main.covidportal.baza.BazaPodataka;

import java.sql.SQLException;
import java.time.LocalTime;

public class UkupanBrojSimptomaNit implements Runnable{

    int sviSimptomi;

    public UkupanBrojSimptomaNit(int sum){
        this.sviSimptomi = sviSimptomi;
    }

    private synchronized void sviSimptomi() {

        while (BazaPodataka.aktivnaVezaSBazomPodataka) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        try {
            BazaPodataka.dohvatiUkupanBrojIzBaze(this.sviSimptomi);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        notifyAll();
    }


    @Override
    public void run() {
        sviSimptomi();
        System.out.println(LocalTime.now());
    }
}
