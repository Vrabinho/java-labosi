package main.covidportal.niti;

import main.covidportal.baza.BazaPodataka;
import main.covidportal.model.Simptom;

import java.io.IOException;
import java.sql.SQLException;

public class DodavanjeNovogSimptomaNit implements Runnable {

    Simptom s;

    public DodavanjeNovogSimptomaNit(Simptom s) {
        this.s = s;
    }

    private synchronized void dodajSimptom() {

        while (BazaPodataka.aktivnaVezaSBazomPodataka) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        try {
            BazaPodataka.spremiNoviSimptom(this.s);
        } catch (SQLException | IOException throwables) {
            throwables.printStackTrace();
        }
        notifyAll();
    }

    @Override
    public void run() {
        dodajSimptom();
    }
}
