package main.covidportal.niti;

import main.covidportal.baza.BazaPodataka;
import main.covidportal.model.Zupanija;

import java.io.IOException;
import java.sql.SQLException;

public class DodavanjeZupanijeNit implements Runnable {

    Zupanija z;

    public DodavanjeZupanijeNit(Zupanija z) {
        this.z = z;
    }

    private synchronized void dodajZupaniju() {

        while (BazaPodataka.aktivnaVezaSBazomPodataka) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        try {
            BazaPodataka.spremiNovuZupaniju(this.z);
        } catch (SQLException | IOException throwables) {
            throwables.printStackTrace();
        }
        notifyAll();
    }

    @Override
    public void run() {
        dodajZupaniju();
    }
}
