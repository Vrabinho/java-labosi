package main.covidportal.niti;

import main.covidportal.baza.BazaPodataka;
import main.covidportal.model.Simptom;

import java.sql.SQLException;
import java.time.LocalTime;

public class ZadnjeUneseniSimptom implements Runnable{

    Simptom s;

    public ZadnjeUneseniSimptom(Simptom b) {
        this.s = s;
    }

    private synchronized void dodajSimptom() {

        while (BazaPodataka.aktivnaVezaSBazomPodataka) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        try {
            BazaPodataka.zadnjiSimptomUBazi(this.s);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        notifyAll();
    }



    @Override
    public void run() {
        dodajSimptom();
        System.out.println(LocalTime.now());
    }
}
