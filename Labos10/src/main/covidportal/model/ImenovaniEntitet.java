package main.covidportal.model;

import java.io.Serializable;
import java.util.Objects;

/**
 * Nadklasa sa parametrom naziv preko koje unosimo nazive za sve objekte
 * koji sadrze parametar naziv
 */

public abstract class ImenovaniEntitet implements Serializable {
    Long id;
    String naziv;

    public ImenovaniEntitet(Long id, String naziv) {
        this.id = id;
        this.naziv = naziv;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ImenovaniEntitet() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ImenovaniEntitet)) return false;
        ImenovaniEntitet that = (ImenovaniEntitet) o;
        return Objects.equals(getNaziv(), that.getNaziv());
    }

    @Override
    public String toString() {
        return "ImenovaniEntitet{" +
                "id=" + id +
                ", naziv='" + naziv + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNaziv());
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }
}
