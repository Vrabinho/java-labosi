package main.covidportal.enumeracija;

public enum Enumeracija {
    RIJETKO("RIJETKO"),
    SREDNJE("SREDNJE"),
    CESTO("ČESTO"),
    PRODUKTIVNI("Produktivni"),
    INTENZIVNO("Intenzivno"),
    VISOKA("Visoka"),
    JAKA("Jaka");

    private String vrijednost;

    Enumeracija(String vrijednost) {
        this.vrijednost = vrijednost;
    }

    public String getVrijednost() {
        return vrijednost;
    }

    @Override
    public String toString() {
        return this.getVrijednost();
    }
}
