package main.java.sample;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import main.covidportal.baza.BazaPodataka;
import main.covidportal.enumeracija.Enumeracija;
import main.covidportal.model.Bolest;
import main.covidportal.model.Simptom;
import main.covidportal.niti.DodavanjeBolestiNit;
import main.covidportal.niti.NajviseZarazenihNit;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class DodavanjeNoveBolestiController implements Initializable {

    static final String FILESIMPTOMI = "dat/simptomi.txt";
    static final String FILEBOLESTI = "dat/bolesti.txt";
    Path datotekaZaPisanje = Path.of(FILEBOLESTI);
    ObservableValue<Integer> idBolesti;
    //Set<Simptom> simptomi = dohvatiSimptome();
    Set<Simptom> simptomi = null;

    @FXML
    private TextField nazivBolesti;

    @FXML
    ListView<Simptom> simptomListView = new ListView<>();


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            simptomi = BazaPodataka.dohvatiSveSimptomeIzBaze();
        } catch (Exception e) {
            e.printStackTrace();
        }
        simptomListView.getItems().addAll(simptomi);
        simptomListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        /*try {
            BufferedReader reader = new BufferedReader(new FileReader(FILEBOLESTI));
            int redak = 0;
            while (reader.readLine() != null) redak++;
            reader.close();

            int id = redak / 3 + 1;
            idBolesti = new SimpleIntegerProperty(id).asObject();

        } catch (IOException ex) {
            ex.printStackTrace();
        }*/
    }

    public void dohvatiBrojRedaka() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(FILEBOLESTI));
            int redak = 0;
            while (reader.readLine() != null) redak++;
            reader.close();

            int id = redak / 3 + 1;
            idBolesti = new SimpleIntegerProperty(id).asObject();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private static Set<Simptom> dohvatiSimptome() {
        Set<Simptom> simptomi = new HashSet<>();
        try (BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(FILESIMPTOMI), StandardCharsets.UTF_8))) {
            String linija = null;
            while ((linija = input.readLine()) != null) {
                Long id = Long.parseLong(linija);
                String naziv = input.readLine();
                String vrijednostSimptoma = input.readLine();
                Enumeracija vrijednostSimptomaEnum = switch (vrijednostSimptoma) {
                    case "RIJETKO" -> Enumeracija.RIJETKO;
                    case "SREDNJE" -> Enumeracija.SREDNJE;
                    case "ČESTO" -> Enumeracija.CESTO;
                    default -> null;
                };
                Simptom tmpSimptom = new Simptom(id, naziv, vrijednostSimptomaEnum);
                simptomi.add(tmpSimptom);
            }

        } catch (IOException ex) {
            System.err.println("Pogreška kod čitanja iz datoteke: " + FILESIMPTOMI);
            ex.printStackTrace();
        }

        return simptomi;
    }

    public String dohvatiVrijednostSimptoma(ListView<Simptom> simptomListView, Set<Simptom> simptomi) {
        ObservableList<Simptom> odabraniSimptomi = simptomListView.getSelectionModel().getSelectedItems();
        List<String> idOdabranihSimptoma = new ArrayList<>();
        StringBuilder id = new StringBuilder();

        for (Simptom odabraniSimptom : odabraniSimptomi) {
            for (Simptom simptom : simptomi) {
                if (odabraniSimptom.getNaziv().equals(simptom.getNaziv())) {
                    idOdabranihSimptoma.add(simptom.getId().toString());
                }
            }
        }

        for (String idOdabranih : idOdabranihSimptoma) {
            id.append(idOdabranih).append(",");
        }

        id = new StringBuilder(id.substring(0, id.length() - 1));
        return id.toString();
    }

    public List<Simptom> dohvatiVrijednostSimptomaList(ListView<Simptom> simptomListView, Set<Simptom> simptomi) {
        ObservableList<Simptom> odabraniSimptomi = simptomListView.getSelectionModel().getSelectedItems();
        List<Simptom> idOdabranihSimptoma = new ArrayList<>();
        StringBuilder id = new StringBuilder();

        for (Simptom odabraniSimptom : odabraniSimptomi) {
            for (Simptom simptom : simptomi) {
                if (odabraniSimptom.getNaziv().equals(simptom.getNaziv())) {
                    idOdabranihSimptoma.add(simptom);
                }
            }
        }

        return idOdabranihSimptoma;
    }

    @FXML
    public void spremiBolest() {

        try {
            dohvatiBrojRedaka();
            String idBolestiWrite = idBolesti.getValue().toString();
            String nazivBolestiWrite = nazivBolesti.getText();
            String vrijednostSimptomaWrite = dohvatiVrijednostSimptoma(simptomListView, simptomi);
            List<Simptom> simptomiLista = dohvatiVrijednostSimptomaList(simptomListView, simptomi);

            Files.writeString(datotekaZaPisanje, "\n" + idBolestiWrite, StandardOpenOption.APPEND);
            Files.writeString(datotekaZaPisanje, "\n" + nazivBolestiWrite, StandardOpenOption.APPEND);
            Files.writeString(datotekaZaPisanje, "\n" + vrijednostSimptomaWrite, StandardOpenOption.APPEND);

            Bolest b = new Bolest(new Long(idBolesti.getValue()), nazivBolestiWrite, simptomiLista);
            DodavanjeBolestiNit dodavanjeBolestiNit = new DodavanjeBolestiNit(b);
            ExecutorService ex = Executors.newFixedThreadPool(1);
            ex.execute(dodavanjeBolestiNit);

            Alert dodaniSimptom = new Alert(Alert.AlertType.INFORMATION);
            dodaniSimptom.setTitle("Dodavanje nove bolesti");
            dodaniSimptom.setHeaderText("Uspješno dodana nova bolest");
            dodaniSimptom.setContentText("Bolest.java " + nazivBolestiWrite + " uspješno spremljena u datoteku.");
            dodaniSimptom.showAndWait();


        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
