package main.java.sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import main.covidportal.baza.BazaPodataka;
import main.covidportal.enumeracija.Enumeracija;
import main.covidportal.model.Simptom;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class PretragaSimptomaController implements Initializable {
    private static ObservableList<Simptom> observableListaSimptoma;
    private static final ObservableList<Simptom> filtriranaObservableListaSimptoma = FXCollections.observableArrayList();
    Boolean prvoPokretanje = true;

    @FXML
    Set<Simptom> simptomi = new HashSet<>();

    @FXML
    private TextField nazivSimptoma;

    @FXML
    private TableView<Simptom> tablicaSimptoma;

    @FXML
    private TableColumn<Simptom, String> stupacNazivSimptoma;

    @FXML
    private TableColumn<Simptom, Enumeracija> stupacVrijednostSimptoma;


    private void dohvatiSimptome() {
        try {
            this.simptomi = BazaPodataka.dohvatiSveSimptomeIzBaze();
        } catch (SQLException | IOException throwables) {
            throwables.printStackTrace();
        }
    }


    @FXML
    public void prikaziGlavniEkran() throws IOException {
        Parent glavniEkranFrame =
                FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("pocetniEkran.fxml")));
        Scene glavniEkranScena = new Scene(glavniEkranFrame, 700, 310);
        Main.getMainStage().setScene(glavniEkranScena);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        stupacNazivSimptoma.setCellValueFactory(new PropertyValueFactory<Simptom, String>("naziv"));
        stupacVrijednostSimptoma.setCellValueFactory(new PropertyValueFactory<Simptom, Enumeracija>("vrijednost"));

        if (observableListaSimptoma == null) {
            observableListaSimptoma = FXCollections.observableArrayList();
        }
        if (prvoPokretanje) {
            observableListaSimptoma.clear();
            dohvatiSimptome();
            observableListaSimptoma.addAll(simptomi);
            tablicaSimptoma.setItems(observableListaSimptoma);
            prvoPokretanje = false;
        }
    }

    @FXML
    public void pretraziSimptome() {
        String naziv = nazivSimptoma.getText();

        List<Simptom> filtriranaListaSimptoma = observableListaSimptoma.stream()
                .filter(simptom -> simptom.getNaziv().toLowerCase().contains(naziv))
                .collect(Collectors.toList());

        filtriranaObservableListaSimptoma.clear();
        filtriranaObservableListaSimptoma.addAll(FXCollections.observableArrayList(filtriranaListaSimptoma));
        tablicaSimptoma.setItems(filtriranaObservableListaSimptoma);
    }
}
