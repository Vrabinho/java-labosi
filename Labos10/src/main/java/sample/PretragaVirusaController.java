package main.java.sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import main.covidportal.enumeracija.Enumeracija;
import main.covidportal.model.Simptom;
import main.covidportal.model.Virus;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

public class PretragaVirusaController implements Initializable {
    static final String FILESIMPTOMI = "dat/simptomi.txt";
    static final String FILEVIRUSI = "dat/virusi.txt";
    private static ObservableList<Virus> observableListaVirusa;
    private static final ObservableList<Virus> filtriranaObservableListaVirusa = FXCollections.observableArrayList();
    Boolean prvoPokretanje = true;

    Set<Simptom> simptomi = new HashSet<>();

    @FXML
    Set<Virus> virusi = new HashSet<>();

    @FXML
    private TextField nazivVirusa;

    @FXML
    private TableView<Virus> tablicaVirusa;

    @FXML
    private TableColumn<Virus, String> stupacNazivVirusa;

    @FXML
    private TableColumn<Virus, Set<Simptom>> stupacSetSimptoma;

    private static void dohvatiSimptome(Set<Simptom> simptomi) {
        try (BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(FILESIMPTOMI), StandardCharsets.UTF_8))) {
            String linija = null;
            while ((linija = input.readLine()) != null) {
                Long id = Long.parseLong(linija);
                String naziv = input.readLine();
                String vrijednostSimptoma = input.readLine();
                Enumeracija vrijednostSimptomaEnum = switch (vrijednostSimptoma) {
                    case "RIJETKO" -> Enumeracija.RIJETKO;
                    case "SREDNJE" -> Enumeracija.SREDNJE;
                    case "ČESTO" -> Enumeracija.CESTO;
                    default -> null;
                };
                Simptom tmpSimptom = new Simptom(id, naziv, vrijednostSimptomaEnum);
                simptomi.add(tmpSimptom);
            }

        } catch (IOException ex) {
            System.err.println("Pogreška kod čitanja iz datoteke: " + FILESIMPTOMI);
            ex.printStackTrace();
        }

    }

    private static void dohvatiViruse(Set<Virus> virusi, Set<Simptom> simptomi) {
        try (BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(FILEVIRUSI), StandardCharsets.UTF_8))) {
            String linija = null;
            while ((linija = input.readLine()) != null) {
                Long id = Long.parseLong(linija);
                String naziv = input.readLine();
                String idSimptoma = input.readLine();
                String[] poljeIdSimptoma = idSimptoma.split(",");
                List<Simptom> simptomiBolesti = new ArrayList<>();
                for (Simptom simptom : simptomi) {
                    for (String s : poljeIdSimptoma) {
                        Long tmpIdSimptoma = Long.parseLong(s);
                        if (simptom.getId().equals(tmpIdSimptoma)) {
                            simptomiBolesti.add(simptom);
                        }
                    }
                }
                Virus tmpVirus = new Virus(id, naziv, (List<Simptom>) simptomiBolesti);
                virusi.add(tmpVirus);

            }
        } catch (IOException ex) {
            System.err.println("Pogreška kod čitanja iz datoteke: " + FILEVIRUSI);
            ex.printStackTrace();
        }
    }


    @FXML
    public void prikaziGlavniEkran() throws IOException {
        Parent glavniEkranFrame =
                FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("pocetniEkran.fxml")));
        Scene glavniEkranScena = new Scene(glavniEkranFrame, 700, 310);
        Main.getMainStage().setScene(glavniEkranScena);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        stupacNazivVirusa.setCellValueFactory(new PropertyValueFactory<Virus, String>("naziv"));
        stupacSetSimptoma.setCellValueFactory(new PropertyValueFactory<Virus, Set<Simptom>>("simptomi"));

        if (observableListaVirusa == null) {
            observableListaVirusa = FXCollections.observableArrayList();
        }
        if (prvoPokretanje) {
            observableListaVirusa.clear();
            dohvatiSimptome(simptomi);
            dohvatiViruse(virusi, simptomi);
            observableListaVirusa.addAll(virusi);
            tablicaVirusa.setItems(observableListaVirusa);
            prvoPokretanje = false;
        }
    }

    @FXML
    public void pretraziViruse() {
        String naziv = nazivVirusa.getText();

        List<Virus> filtriranaListaVirusa = observableListaVirusa.stream()
                .filter(virus -> virus.getNaziv().toLowerCase().contains(naziv))
                .collect(Collectors.toList());

        filtriranaObservableListaVirusa.clear();
        filtriranaObservableListaVirusa.addAll(FXCollections.observableArrayList(filtriranaListaVirusa));
        tablicaVirusa.setItems(filtriranaObservableListaVirusa);
    }
}
