package main.java.sample;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Duration;
import main.covidportal.baza.BazaPodataka;
import main.covidportal.model.Zupanija;
import main.covidportal.niti.NajviseZarazenihNit;


import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class PretragaZupanijaController implements Initializable {
    private static ObservableList<Zupanija> observableListaZupanija;
    private static final ObservableList<Zupanija> filtriranaObservableListaZupanija = FXCollections.observableArrayList();
    Boolean prvoPokretanje = true;
    private static final int BROJ_NITI = 1;

    @FXML
    List<Zupanija> zupanije = new ArrayList<>();

    @FXML
    private TextField nazivZupanije;

    @FXML
    private TableView<Zupanija> tablicaZupanija;

    @FXML
    private TableColumn<Zupanija, String> stupacNazivZupanije;

    @FXML
    private TableColumn<Zupanija, Integer> stupacBrojStanovnika;

    @FXML
    private TableColumn<Zupanija, Integer> stupacBrojZarazenih;


    private void dohvatiZupanijeBaza() {
        try {
            this.zupanije = BazaPodataka.dohvatiSveZupanijeIzBaze();
        } catch (SQLException | IOException throwables) {
            throwables.printStackTrace();
        }
    }


    @FXML
    public void prikaziGlavniEkran() throws IOException {
        Parent glavniEkranFrame =
                FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("pocetniEkran.fxml")));
        Scene glavniEkranScena = new Scene(glavniEkranFrame, 700, 310);
        Main.getMainStage().setScene(glavniEkranScena);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        stupacNazivZupanije.setCellValueFactory(new PropertyValueFactory<Zupanija, String>("naziv"));
        stupacBrojStanovnika.setCellValueFactory(new PropertyValueFactory<Zupanija, Integer>("brojStanovnika"));
        stupacBrojZarazenih.setCellValueFactory(new PropertyValueFactory<Zupanija, Integer>("brojZarazenih"));

        if (observableListaZupanija == null) {
            observableListaZupanija = FXCollections.observableArrayList();
        }
        if (prvoPokretanje) {
            observableListaZupanija.clear();
            dohvatiZupanijeBaza();
            observableListaZupanija.addAll(zupanije);
            tablicaZupanija.setItems(observableListaZupanija);
            prvoPokretanje = false;
        }

        Timeline timeline = new Timeline(
                new KeyFrame(Duration.seconds(10), e -> {
                    List<Zupanija> z = null;
                    try {
                        z = BazaPodataka.dohvatiSveZupanijeIzBaze();
                    } catch (SQLException | IOException throwables) {
                        throwables.printStackTrace();
                    }
                    Zupanija maxZup = z.stream()
                            .max(Comparator.comparing(Zupanija::getPostotakZarazenih))
                            .get();
                    Main.getMainStage().setTitle(maxZup.getNaziv());
                })
        );
        timeline.play();

        NajviseZarazenihNit najviseZarazenihNit = new NajviseZarazenihNit();
        ExecutorService ex = Executors.newFixedThreadPool(BROJ_NITI);
        ex.execute(najviseZarazenihNit);
    }

    @FXML
    public void pretraziZupanije() {
        String naziv = nazivZupanije.getText();

        List<Zupanija> filtriranaListaZupanija = observableListaZupanija.stream()
                .filter(zupanija -> zupanija.getNaziv().toLowerCase().contains(naziv))
                .collect(Collectors.toList());

        filtriranaObservableListaZupanija.clear();
        filtriranaObservableListaZupanija.addAll(FXCollections.observableArrayList(filtriranaListaZupanija));
        tablicaZupanija.setItems(filtriranaObservableListaZupanija);
    }
}