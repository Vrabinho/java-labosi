package hr.java.covidportal.main;

import hr.java.covidportal.model.Bolest;
import hr.java.covidportal.model.Osoba;
import hr.java.covidportal.model.Simptom;
import hr.java.covidportal.model.Zupanija;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Glavna {

    static final int BROJ_ZUPANIJA = 3;
    static final int BROJ_OSOBA = 3;
    static final int BROJ_SIMPTOMA = 3;
    static final int BROJ_BOLESTI = 3;
    static final int MAX_STANOVNIKA = 10000000;

    public static void main(String[] args) {

        // klasa za unos podataka sa tipkovnice
        Scanner unos = new Scanner(System.in);

        //petlja za unos zupanija
        Zupanija[] zupanije = new Zupanija[BROJ_ZUPANIJA];
        System.out.println("Unesite podatke o " + BROJ_ZUPANIJA + " županije: ");

        Integer maxZupanija = 0;
        Integer minZupanija = MAX_STANOVNIKA;
        int indexZupanijeMax = 0;
        int indexZupanijeMin = 0;

        for (int i = 0; i < BROJ_ZUPANIJA; i++) {
            zupanije[i] = unosZupanije(unos);
            unos.nextLine();
            if(zupanije[i].getBrojStanovnika()>maxZupanija){
                maxZupanija = zupanije[i].getBrojStanovnika();
                indexZupanijeMax = i;
            }
            else if (zupanije[i].getBrojStanovnika()<minZupanija){
                minZupanija = zupanije[i].getBrojStanovnika();
                indexZupanijeMin = i;
            }

        }

        //petlja za unos simptoma
        Simptom[] simptomi = new Simptom[BROJ_SIMPTOMA];
        System.out.println("Unesite podatke o " + BROJ_SIMPTOMA + " simptoma");

        for (int i = 0; i < BROJ_SIMPTOMA; i++) {
            simptomi[i] = unosSimptoma(unos);
        }

        //petlja za unos simptoma
        Bolest[] bolesti = new Bolest[BROJ_BOLESTI];
        System.out.println("Unesite podatke o " + BROJ_BOLESTI + " bolesti:");
        for (int i = 0; i < BROJ_BOLESTI; i++) {
            bolesti[i] = unosBolesti(unos, simptomi);
        }

        //petlja za unos osoba
        Osoba[] osobe = new Osoba[BROJ_OSOBA];
        for (int i = 0; i < BROJ_OSOBA; i++) {
            osobe[i] = unosOsobe(unos, bolesti, zupanije, i);
            if (i > 0) {
                for(int j = 0; j< i; j++){
                    if(osobe[i].getJmbg().equals(osobe[j].getJmbg())){
                        System.out.println("Pogrešan unos osobe, jmbg se ponavlja! Unesite ponovo podatke za osobu:");
                        osobe[i] = unosOsobe(unos, bolesti, zupanije, i);
                    }
                }
                unosKontaktiranihOsoba(unos, osobe, i);
            }
        }
        ispisOsoba(osobe, zupanije, indexZupanijeMax, indexZupanijeMin);
        unos.close();
    }


    //UNOSI
    //unos zupanije - metoda
    private static Zupanija unosZupanije(Scanner unos) {
        System.out.print("Unesite naziv županije: ");
        String nazivZupanije = unos.nextLine();
        System.out.print("Unesite broj stanovnika: ");
        Integer brojStanovnika = unos.nextInt();
        return new Zupanija(nazivZupanije, brojStanovnika);
    }

    //unos simptoma - metoda
    private static Simptom unosSimptoma(Scanner unos) {
        System.out.print("Unesite naziv simptoma: ");
        String nazivSimptoma = unos.nextLine();
        System.out.print("Unesite vrijednost simptoma (RIJETKO, SREDNJE ILI ČESTO): ");
        String vrijednostSimptoma = unos.nextLine();
        return new Simptom(nazivSimptoma, vrijednostSimptoma);
    }

    //Unos bolesti - metoda
    private static Bolest unosBolesti(Scanner unos, Simptom[] simptomi) {
        System.out.print("Unesite naziv bolesti: ");
        String nazivBolesti = unos.nextLine();
        System.out.print("Unesite broj simptoma: ");
        int brojSimptoma = unos.nextInt();
        Simptom[] simptomiBolesti = new Simptom[brojSimptoma];
        for (int j = 0; j < brojSimptoma; j++) {
            System.out.println("Odaberite " + (j + 1) + " simptom:");
            simptomiBolesti[j] = odabirSimptoma(unos, simptomi);
        }
        return new Bolest(nazivBolesti, simptomiBolesti);
    }

    //Unos Osoba - metoda
    private static Osoba unosOsobe(Scanner unos, Bolest[] bolesti, Zupanija[] zupanije, int i) {
        System.out.print("Unesite ime "+ (i + 1) + ". osobe: ");
        String ime = unos.nextLine();
        System.out.print("Unesite prezime osobe: ");
        String prezime = unos.nextLine();
        String jmbg;
        String zadaniParametri = "^[0-9]{13}$";
        Pattern provjera = Pattern.compile(zadaniParametri);
        Matcher provjeraStringa;
        do {
            System.out.print("Uneste JMBG: ");
            jmbg = unos.nextLine();
            Matcher matcher = provjera.matcher(jmbg);
            provjeraStringa = matcher;
            if(!provjeraStringa.find()){
                System.out.println("Neispravan unos, molimo pokušajte ponovno! ");
            }
        }while(provjeraStringa.find());
        System.out.print("Unesite starost osobe: ");
        Integer starost = unos.nextInt();
        unos.nextLine();
        System.out.println("Unesite županiju prebivališta osobe:");
        Zupanija zupanijaOsobe = odabirZupanije(unos, zupanije);
        System.out.println("Unesite bolest osobe:");
        Bolest bolestOsobe = odabirBolesti(unos, bolesti);
        return new Osoba(ime, prezime, jmbg, starost, zupanijaOsobe, bolestOsobe);
        }


    //ODABIRI
    //odabir simptoma - metoda
    private static Simptom odabirSimptoma(Scanner unos, Simptom[] simptomi) {
        int odabraniSimptom;
        do {
            for (int k = 0; k < BROJ_SIMPTOMA; k++) {
                System.out.println(k + 1 + ". " + simptomi[k].getNaziv() + " " + simptomi[k].getVrijednost());
            }
            System.out.print("Odabir: ");
            odabraniSimptom = unos.nextInt();
            if (odabraniSimptom > BROJ_SIMPTOMA) {
                System.out.println("Neispravan unos, molimo pokušajte ponovno! ");
            }
        } while (odabraniSimptom <= 0 || odabraniSimptom > BROJ_SIMPTOMA);
        unos.nextLine();
        return simptomi[odabraniSimptom - 1];
    }

    //Odabir bolesti
    private static Bolest odabirBolesti(Scanner unos, Bolest[] bolesti) {
        for (int i = 0; i < BROJ_BOLESTI; i++) {
            System.out.println(i + 1 + ". " + bolesti[i].getNaziv());
        }
        System.out.print("Odabir: ");
        int odabranaBolest = unos.nextInt();
        unos.nextLine();
        return bolesti[odabranaBolest - 1];
    }

    //Odabir zupanije
    private static Zupanija odabirZupanije(Scanner unos, Zupanija[] zupanije) {
        for (int i = 0; i < BROJ_ZUPANIJA; i++) {
            System.out.println(i + 1 + ". " + zupanije[i].getNaziv());
        }
        System.out.print("Odabir: ");
        int odabranaZupanija = unos.nextInt();
        unos.nextLine();
        return zupanije[odabranaZupanija - 1];
    }

    //Odabir kontaktirane osobe
    private static void unosKontaktiranihOsoba(Scanner unos, Osoba[] osobe, int i) {
            System.out.println("Unesite broj osoba koje su bile u kontaktu s tom osobom: ");
            int brojKontaktiranihOsoba = unos.nextInt();
            unos.nextLine();
            Osoba[] tmp = new Osoba[brojKontaktiranihOsoba];
            for(int j=0; j<brojKontaktiranihOsoba; j++){
                System.out.println("Odaberite " + (j + 1) + " osobu:");
                for (int k = 0; k < brojKontaktiranihOsoba; k++) {
                    System.out.println(k + 1 + "." + osobe[k].getIme() + " " + osobe[k].getPrezime());
                }
                System.out.print("Odabir: ");
                int odabranaOsoba = unos.nextInt();
                unos.nextLine();
                tmp[j] = osobe[odabranaOsoba-1];
            }
            osobe[i].setKontaktiraneOsobe(tmp);
    }

    //ISPIS
    private static void ispisOsoba(Osoba[] osobe, Zupanija[] zupanije, int maxZupanija, int minZupanija) {
        System.out.println("Popis osoba: ");
        for(int i = 0; i< BROJ_OSOBA; i++){
            System.out.println("Ime i prezime: "+ osobe[i].getIme() + " " + osobe[i].getPrezime());
            System.out.println("JMBG: " + osobe[i].getJmbg());
            System.out.println("Starost: " + osobe[i].getStarost());
            System.out.println("Županija prebivališta: " + osobe[i].getZupanija().getNaziv());
            System.out.println("Zaražen bolešću: " + osobe[i].getZarazenBolescu().getNaziv());
            System.out.println("Kontaktirane osobe:");
            if(osobe[i].getKontaktiraneOsobe() != null) {
                for (int j = 0; j < osobe[i].getKontaktiraneOsobe().length; j++) {
                    System.out.println(osobe[i].getKontaktiraneOsobe()[j].getIme() + " " + osobe[i].getKontaktiraneOsobe()[j].getPrezime());
                }
            }else
                System.out.println("Nema kontaktiranih osoba!");
        }
        System.out.println("Zupanija sa najmanje stanovnika: " + zupanije[minZupanija].getNaziv());
        System.out.println("Zupanija sa najviše stanovnika: " + zupanije[maxZupanija].getNaziv());
    }
}
