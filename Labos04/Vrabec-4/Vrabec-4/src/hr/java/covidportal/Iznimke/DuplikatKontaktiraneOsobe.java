package hr.java.covidportal.Iznimke;

/**
 * Usporedba duplog unosa kontaktirane osobe
 */

public class DuplikatKontaktiraneOsobe extends Exception{

    /**
     *konstruktor sa jednim parametrom
     * @param message prima poruku iznimke
     */
    public DuplikatKontaktiraneOsobe(String message) {
        super(message);
    }

    /**
     * konstruktor sa dva parametra
     * @param message prima poruku iznimke
     * @param cause prima uzrok iznimke
     */
    public DuplikatKontaktiraneOsobe(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * konstruktor sa jednim parametrom
     * @param cause prima uzrok iznimke
     */
    public DuplikatKontaktiraneOsobe(Throwable cause) {
        super(cause);
    }
}
