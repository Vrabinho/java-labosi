package hr.java.covidportal.model;

import java.util.Arrays;

/**
 * prima polje simptoma
 */

public class Bolest extends ImenovaniEntitet  {
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Bolest)) return false;
        Bolest bolest = (Bolest) o;
        return Arrays.equals(getSimptomi(), bolest.getSimptomi());
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(getSimptomi());
    }

    Simptom[] simptomi;

    /**
     * konstruktor sa dva parametra
     * @param naziv prima naziv bolesti koji saljemo u nadklasu
     * @param simptomi prima polje simptoma
     */

    public Bolest(String naziv, Simptom[] simptomi) {
        super(naziv);
        this.simptomi = simptomi;
    }


    public Simptom[] getSimptomi() {
        return simptomi;
    }

    public void setSimptomi(Simptom[] simptomi) {
        this.simptomi = simptomi;
    }
}
