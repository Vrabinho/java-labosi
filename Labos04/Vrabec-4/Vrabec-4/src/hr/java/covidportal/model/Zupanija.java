package hr.java.covidportal.model;

import java.util.Objects;

/**
 * Extenda <code>ImenovaniEntitet</code>
 */

public class Zupanija extends ImenovaniEntitet{


    Integer brojStanovnika;
    Integer brojZarazenih;
    Float postotakZarazenih;

    /**
     * Konstruktor sa dva parametra
     * @param naziv prima naziv zupanije i proslijeduje ga nadklasi <code>ImenovaniEntitet</code>
     * @param brojStanovnika prima integer
     */

    public Zupanija(String naziv, Integer brojStanovnika, Integer brojZarazenih) {
        super(naziv);
        this.brojStanovnika = brojStanovnika;
        this.brojZarazenih = brojZarazenih;
        this.postotakZarazenih = ((float)(brojStanovnika/brojZarazenih));
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Zupanija zupanija)) return false;
        return getBrojStanovnika().equals(zupanija.getBrojStanovnika());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBrojStanovnika());
    }

    public Integer getBrojZarazenih() {
        return brojZarazenih;
    }

    public void setBrojZarazenih(Integer brojZarazenih) {
        this.brojZarazenih = brojZarazenih;
    }

    public Float getPostotakZarazenih() { return postotakZarazenih; }

    public Integer getBrojStanovnika() {
        return brojStanovnika;
    }

    public void setBrojStanovnika(Integer brojStanovnika) {
        this.brojStanovnika = brojStanovnika;
    }
}
