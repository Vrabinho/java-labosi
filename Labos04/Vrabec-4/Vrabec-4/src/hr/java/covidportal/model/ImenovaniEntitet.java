package hr.java.covidportal.model;

import java.util.Objects;

/**
 * Nadklasa sa parametrom naziv preko koje unosimo nazive za sve objekte
 * koji sadrze parametar naziv
 */

public abstract class ImenovaniEntitet {
    String naziv;

    /**
     * konstruktor sa jednim parametrom
     * @param naziv prima naziv objekta
     */
    public ImenovaniEntitet(String naziv) {
        this.naziv = naziv;
    }

    public ImenovaniEntitet() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ImenovaniEntitet)) return false;
        ImenovaniEntitet that = (ImenovaniEntitet) o;
        return Objects.equals(getNaziv(), that.getNaziv());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNaziv());
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }
}
